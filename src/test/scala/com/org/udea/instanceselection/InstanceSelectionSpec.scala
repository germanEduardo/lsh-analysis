package com.org.udea.instanceselection

import com.org.udea.SparkTestWrapper
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class InstanceSelectionSpec  extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  feature("get the minority class label") {

    scenario("unbalanced dataframes") {

      Given("an Unbalanced DataFrame with label '1' as minority label " +
        "and other with '0' and the arguments with an error in the method")
      val method = "instance_Selection"
      val instances = someDF
      val unbalanced = true
      val neighbors = 3
      val subBuckets = 1
      val distancesIntervale = 500

      When(message = "I call the method minorityClass")

      val minorityLabel_one = InstanceSelection.minorityClass(unBalancedDF_one)
      val minorityLabel_zero = InstanceSelection.minorityClass(unBalancedDF_zero)


      Then("the label compute is '1'")
      assert(minorityLabel_one === 1)

      And("the label compute is '0'")
      assert(minorityLabel_zero === 0)

      And("using the unimplemmented IS method should trigger IllegalArgumentException")
      assertThrows[IllegalArgumentException] {
        InstanceSelection.instanceSelection(method, instances, spark, unbalanced,
          neighbors, subBuckets, distancesIntervale)
      }
    }
  }

}
