package com.org.udea.instanceselection

import com.org.udea.SparkTestWrapper
import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Constants
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RandomSamplingSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper {

  feature("over sampling minority class.") {

    scenario("minority") {

      Given(
        "an Unbalanced DataFrame with label '1' as minority label " +
          "and other with '0' and the arguments with an error in the method"
      )

      val params = new IsParams(
        unBalancedDF_one,
        true,
        1,
        spark,
        1,
        1,
        1
      )

      When(message = "I call the method OverSampling")

      val randomSampled = RandomSampling.OverSampling(params)

      Then("minority class is oversampled")
      val minorCount =
        randomSampled.filter(randomSampled(Constants.COL_LABEL) === 1).count()
      assert(minorCount === 4)

      Then("majority class is the same")
      val majorCount =
        randomSampled.filter(randomSampled(Constants.COL_LABEL) === 0).count()
      assert(majorCount === 4)

    }

    scenario("minority not even") {

      Given(
        "an Unbalanced DataFrame with label '1' as minority label " +
          "and other with '0' with ratio uneven"
      )

      val params = new IsParams(
        test_unbalancedDF,
        true,
        1,
        spark,
        1,
        1,
        1
      )

      When(message = "I call the method OverSampling")

      val randomSampled = RandomSampling.OverSampling(params)

      Then("minority class is oversampled")
      assert(
        randomSampled
          .filter(randomSampled(Constants.COL_LABEL) === 1)
          .count() === 9
      )

      Then("majority class is the same")
      assert(
        randomSampled
          .filter(randomSampled(Constants.COL_LABEL) === 0)
          .count() === 11
      )

    }
  }

  feature("under-sampling minority class.") {

    scenario("even minority") {

      Given(
        "an Unbalanced DataFrame with label '1' as minority label " +
          "and other with '0' and the arguments with an error in the method"
      )

      val params = new IsParams(
        unBalancedDF_one,
        true,
        1,
        spark,
        1,
        1,
        1
      )

      When(message = "I call the method OverSampling")

      val randomSampled = RandomSampling.UnderSampling(params, 1)

      Then("minority class is the same")

      assert(
        randomSampled
          .filter(randomSampled(Constants.COL_LABEL) === 1)
          .count() === 2
      )

      Then("majority class is undersampled")
      assert(
        randomSampled
          .filter(randomSampled(Constants.COL_LABEL) === 0)
          .count() <= 3
      )

    }

    scenario("minority not even") {

      Given(
        "an Unbalanced DataFrame with label '1' as minority label " +
          "and other with '0' with ratio uneven"
      )

      val params = new IsParams(
        test_unbalancedDF,
        true,
        1,
        spark,
        1,
        1,
        1
      )

      When(message = "I call the method OverSampling")

      val randomSampled = RandomSampling.UnderSampling(params)

      Then("minority class is the same")
      assert(
        randomSampled
          .filter(randomSampled(Constants.COL_LABEL) === 1)
          .count() === 3
      )

      Then("majority class is undersampled")
      val majorCount =
        assert(
          randomSampled
            .filter(randomSampled(Constants.COL_LABEL) === 0)
            .count() <= 4
        )

    }
  }

}
