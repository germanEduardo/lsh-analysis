package com.org.udea.instanceselection

import com.org.udea.SparkTestWrapper
import com.org.udea.instanceselection.EntropySampling.addEntropy
import org.apache.spark.mllib.tree.impurity.Entropy
import com.org.udea.lsh.{RandomHyperplanesLSH, DotProductLSH}
import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Constants
import org.apache.log4j.Logger
import org.apache.spark.ml.linalg.Vectors
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}
import org.apache.spark.sql.Row

class EntropySamplingSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{

  val aggEntropy = new AggEntropyUnbalanced()

  feature ("entropy") {
    scenario("entropy for single class df") {

      Given("DF with one class")
      oneClassDF.collect.foreach(println)
      When("entropy is computed")
      val entropyResults = addEntropy(
        oneClassDF,
        (Constants.COL_SIGNATURE,
          Constants.COL_LABEL,
          Constants.COL_ENTROPY),
        aggEntropy)
      Then("the entropy is computed as the probability to select one sample")
      entropyResults.collect.foreach(println)
      val finalEntropy = entropyResults.select(Constants.COL_ENTROPY).take(1).map(_.getDouble(0))
      assert(finalEntropy(0) == (1.0/oneClassDF.count()))
    }


  }


  feature(" Entropy sampling based with RHP") {
    val numHashTables = 2
    val hyperplanes = Array(
      Vectors.dense(0, 1),
      Vectors.dense(1, 0))

    scenario("compute IS with entropy and Random hyperplanes as LSH With balanced DF") {

      Given("A dataframe balanced and a lsh computed")

      val hyp = new RandomHyperplanesLSH(test_balancedDF, numHashTables, spark)
      hyp.setHyperplanes(hyperplanes)
      val instancesWithSignature = hyp.lsh(Constants.COL_FEATURES)

      When("the instance selection sampling is applied")

      val entropyForSignature = addEntropy(
        instancesWithSignature.toDF(),
        (Constants.COL_SIGNATURE,
          Constants.COL_LABEL,
          Constants.COL_ENTROPY),
        aggEntropy)
      Logger.getRootLogger().info(instancesWithSignature.show)
      Logger.getRootLogger().info(entropyForSignature.show)
      val entropies: Array[Row] = entropyForSignature.take(4)

      val params = new IsParams(instancesWithSignature, false, 1 , spark,
        0, 1000, 0)

      val dfSampled = EntropySampling.instanceSelection(params)

      Logger.getRootLogger().info("df sampled")
      Logger.getRootLogger().info(dfSampled.show)

      Then("the entropy of the signatures is:")
      assert(entropies.filter(_.getString(0)=="11")(0).getDouble(1) ===
        Entropy.calculate(Array(2,3),5))
      assert(entropies.filter(_.getString(0)=="01")(0).getDouble(1)===
        Entropy.calculate(Array(2,1),3))

      And("the bucket with the same samples of each class, the entropy have to be 1.0")
      assert(entropies.filter(_.getString(0)=="10")(0).getDouble(1) === 1.0 )

      And ("in the case of bucket with the same class, the entropy have to be 1/numOfSamples")
      assert(entropies.filter(_.getString(0)=="00")(0).getDouble(1) === 1.0/2.0)

      And ("at least the sampled DF have to be equal or less")
      assert(dfSampled.count() <= instancesWithSignature.count())


     }


    scenario("compute IS with entropy and Random hyperplanes as LSH With unbalanced DF") {

      Given("A unbalanced dataframe and a lsh computed")

      val hyp = new RandomHyperplanesLSH(test_unbalancedDF, numHashTables, spark)
      hyp.setHyperplanes(hyperplanes)
      val instancesWithSignature = hyp.lsh(Constants.COL_FEATURES)

      When("the instance selection sampling is applied")

      val entropyForSignature = addEntropy(
        instancesWithSignature.toDF(),
        (Constants.COL_SIGNATURE,
          Constants.COL_LABEL,
          Constants.COL_ENTROPY),
        aggEntropy)

      Logger.getRootLogger().info(entropyForSignature.show)
      val entropies: Array[Row] = entropyForSignature.take(4)

      val params = new IsParams(instancesWithSignature, true, 1 , spark,
        0, 1000, 0)

      val dfSampled = EntropySampling.instanceSelection(params)

      Logger.getRootLogger().info("df sampled")
      Logger.getRootLogger().info(dfSampled.show)

      Then("the entropy of the signatures is:")
      assert(entropies.filter(_.getString(0)=="11")(0).getDouble(1) ===
        Entropy.calculate(Array(4,1),5))
      assert(entropies.filter(_.getString(0)=="01")(0).getDouble(1)===
        Entropy.calculate(Array(2,1),3))

      And("the bucket with the same samples of each class, the entropy have to be 1.0")
      assert(entropies.filter(_.getString(0)=="00")(0).getDouble(1) === 1.0 )

      And ("in the case of bucket with the same class, the entropy have to be 1/numOfSamples")
      assert(entropies.filter(_.getString(0)=="10")(0).getDouble(1) === 1.0/4.0)

      And ("the sampled DF have to be less")
      assert(dfSampled.count() < instancesWithSignature.count())

      And ("the sampled DF have to have the same samples of the minority class that original")
      assert(dfSampled.where(Constants.COL_LABEL+"==1").count() ===
        instancesWithSignature.where(Constants.COL_LABEL+"==1").count())
    }


  }

  feature(" Entropy sampling based with PStables") {
    val numHashTables = 2

    val familieFunctions = Seq(List((Vectors.dense(1.0, 0.0), 1.0),
      (Vectors.dense(-1.0, 0), 0.0)))

    val andsFunctions = 2
    val orsFunctions = 1


    scenario("compute IS with entropy and P-satables as LSH With balanced DF") {

      Given("A dataframe balanced and a lsh computed")

      val rp = new DotProductLSH(test_balancedDF, andsFunctions, orsFunctions, 0.5, spark)
      rp.setFamilies(familieFunctions)

      val instancesWithSignature = rp.lsh(Constants.COL_FEATURES)

      When("the instance selection sampling is applied")

      val entropyForSignature = addEntropy(
        instancesWithSignature.toDF(),
        (Constants.COL_SIGNATURE,
          Constants.COL_LABEL,
          Constants.COL_ENTROPY),
        aggEntropy)

      Logger.getRootLogger().info(entropyForSignature.show)

      val entropies: Array[Row] = entropyForSignature.take(5)

      val params = new IsParams(instancesWithSignature, false, 1, spark,
        0, 1000, 0)

      val dfSampled = EntropySampling.instanceSelection(params)

      Logger.getRootLogger().info("df sampled")
      Logger.getRootLogger().info(dfSampled.show)

      Then("the entropy of the signatures is:")
      assert(entropies.filter(_.getString(0)=="011")(0).getDouble(1) ===
        Entropy.calculate(Array(2,1),3))

      And("the bucket with the same samples of each class, the entropy have to be 1.0")
      assert(entropies.filter(_.getString(0)=="020")(0).getDouble(1) === 1.0 )

      And ("at least the sampled DF have to be equal or less")
      assert(dfSampled.count() <= instancesWithSignature.count())

    }

    scenario("compute IS with entropy and P-satables as LSH With unbalanced DF") {

      Given("A dataframe unbalanced and a lsh computed")

      val rp = new DotProductLSH(test_unbalancedDF, andsFunctions, orsFunctions, 0.5, spark)
      rp.setFamilies(familieFunctions)

      val instancesWithSignature = rp.lsh(Constants.COL_FEATURES)

      When("the instance selection sampling is applied")

      val entropyForSignature = addEntropy(
        instancesWithSignature.toDF(),
        (Constants.COL_SIGNATURE,
          Constants.COL_LABEL,
          Constants.COL_ENTROPY),
        aggEntropy)

      Logger.getRootLogger().info(entropyForSignature.show)
      val entropies: Array[Row] = entropyForSignature.take(5)

      val params = new IsParams(instancesWithSignature, true, 1, spark,
        0, 1000, 0)

      val dfSampled = EntropySampling.instanceSelection(params)

      Logger.getRootLogger().info("df sampled")
      Logger.getRootLogger().info(dfSampled.show)

     Then("the entropy of the signatures is:")
      assert(entropies.filter(_.getString(0)=="011")(0).getDouble(1) ===
        Entropy.calculate(Array(2,1),3))

      And("the bucket with the same samples of each class, the entropy have to be 1.0")
      assert(entropies.filter(_.getString(0)=="020")(0).getDouble(1) === 1.0 )


      And("the bucket with one class have to 1/numofsamples")
      assert(entropies.filter(_.getString(0)=="002")(0).getDouble(1) === 1.0/3.0 )

      And ("sampled DF have to be less")
      assert(dfSampled.count() < instancesWithSignature.count())

      And ("the sampled DF have to have the same samples of the minority class that original")
      assert(dfSampled.where(Constants.COL_LABEL+"==1").count() ===
        instancesWithSignature.where(Constants.COL_LABEL+"==1").count())
    }

  }

}
