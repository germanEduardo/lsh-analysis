package com.org.udea.instanceselection

import com.org.udea.SparkTestWrapper
import com.org.udea.lsh.LshMethods
import org.apache.log4j.Logger
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class LshMethodsSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  feature("subBuckets performing") {

    scenario("an unnormalize dataframes") {

    Given("an unNormalize")

      val df = spark.read.parquet("data/dfToTestSubucket1")
      val maxBucketSize = 5
      val expectedSignatures = 2

      When(message = "I call the method subBuckets")

      Logger.getRootLogger().info(df.show)
      Logger.getRootLogger().info("numpar " + df.rdd.getNumPartitions)

      val instances = LshMethods.subBuckets(maxBucketSize, df, spark)

      Logger.getRootLogger().info(instances.show)
      Logger.getRootLogger().info("numpar " + instances.rdd.getNumPartitions)

      Then("the number of signatures is " + expectedSignatures)

      assert(LshMethods.getKeys(instances).count() === expectedSignatures)

      And("the columns should be the same")

      assert(instances.columns === df.columns )


    }

  }

  scenario("a dataframe with number of signatures diferrent") {

    Given("an unNormalize dataframe with the signatures different number of samples with signature")
    /* the DF have the folliwng saples by signature:
    "1" => 2
    "2" => 6
    "3" => 6
    "5" => 1
    */
    val maxBucketSize = 2

    When(message = "I call the method subBuckets")

    Logger.getRootLogger().info(dfForTestSubBucket.show)

    val instances = LshMethods.subBuckets(maxBucketSize, dfForTestSubBucket, spark)

    Logger.getRootLogger().info(instances.show)

    Logger.getRootLogger().info(LshMethods.getKeys(instances).count())

    Then("the number of signatures is  have to be more")
    assert(true)

    assert(LshMethods.getKeys(instances).count() >= LshMethods.getKeys(dfForTestSubBucket).count())

    And("the columns should be the same")

    assert(instances.columns === dfForTestSubBucket.columns )

    And ("the dataframe result have to have 14 samples")

    assert(instances.count === 15 )

  }


}
