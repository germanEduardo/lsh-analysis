package com.org.udea.utilities.structures

import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class TableSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter {

  val table = new Table()
  table.addRow(RowTable(Id(1, 1), null, null, 0.0, List()))
  table.addRow(RowTable(Id(2, 1), null, null, 0.0, List()))
  table.addRow(RowTable(Id(3, -1), null, null, 0.0, List()))



  feature("Get ID in the table") {

    scenario("existing row in the table") {

      Given("a table and existing id in the table")
      val existingId = 1

      When("I try to get the row")

      val (index, row )= table.getIndexAndRowById(id = existingId)

      Then("I get row with the specific id")
      assert(index == 0 )
      assert(row == RowTable(Id(1, 1), null, null, 0.0, List()) )

    }

    scenario("non existing row in the table")
    {
      Given("non exisiting id in the table")
      val nonExistingId = 5
      When("I try to get the row with the specific id")

      Then("the execption is raised")

      assertThrows[NotExistingRow] {
        table.getIndexAndRowById(id = nonExistingId)}

    }

  }

}
