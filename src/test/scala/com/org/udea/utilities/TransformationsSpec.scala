package com.org.udea.utilities

import com.org.udea.SparkTestWrapper
import org.apache.log4j.Logger
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

import scala.io.Source

class TransformationsSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper {

  feature("Dataframe Normalization of the features") {

    scenario("normalize DataFrame") {

      Given("a desnormalized DataFrame")
      val someDF = spark.createDataFrame(
        spark.sparkContext.parallelize(someData),
        StructType(idFeaturesLabelScheme)
      )

      When(message = "normalize is called")
      val result: DataFrame =
        Transformations.normalize(df = someDF, inputCol = "features")

      /* TODO:
      Then("the resulted dataframe have to be normalized")
      pending
       */

      Then("have to have a column called " + Constants.COL_SCALED)
      assert(Transformations.hasColumn(result, Constants.COL_SCALED))

    }
  }

  feature("index manual categories") {

    scenario("index features with jsonFile") {

      Given("a dataframe with index and jsonfile with categories")
      val jsonFileToTest =
        "src/test/resources/testIndex.json"

      When(message = "indexer is called")
      val result: DataFrame =
        Transformations.dfManualIndex(test_balancedDF, jsonFileToTest)

      /* TODO:
      Then("evaluate truly indexed")
      pending
       */
      Then(" dataframe have a column called " + Constants.COL_INDEXED_FEATURES)
      assert(Transformations.hasColumn(result, Constants.COL_INDEXED_FEATURES))

    }
  }

  feature("get individuals columns for a feature vector") {

    scenario("a formatted Dataframe") {

      Given(
        "a dataframe with the correct format and number of features and columns"
      )
      val numOfFeatures =
        dataToTransform(0).getAs[org.apache.spark.ml.linalg.Vector](1).size
      val fistVector =
        dataToTransform(0).getAs[org.apache.spark.ml.linalg.Vector](1).toArray
      val numOfCols = dataToTransformDF.columns.size

      When(message = "the transformation is called")
      val result: DataFrame =
        Transformations.getColsFromFeature(dataToTransformDF)

      val numOfColsTransformed = numOfFeatures + numOfCols
      val RowWithFeaAsCols =
        result.select("Feature0", "Feature1", "Feature2", "Feature3").first()
      Then(
        " the dataframe have to have # number of columns: " + numOfColsTransformed
      )
      assert(result.columns.size == numOfColsTransformed)

      And("the columns have to have the values: ")
      assert(fistVector(0) == RowWithFeaAsCols.getDouble(0))
      assert(fistVector(1) == RowWithFeaAsCols.getDouble(1))
      assert(fistVector(2) == RowWithFeaAsCols.getDouble(2))
      assert(fistVector(3) == RowWithFeaAsCols.getDouble(3))

    }
  }

  feature("check if the column with id have is unique") {
    scenario("dataframe wiht unique ids") {
      Given("a dataframe with unique id defined")

      val uniqueIdDF = spark.createDataFrame(
        spark.sparkContext.parallelize(
          Seq(
            Row(1, Vectors.dense(Array(1.0, 2.0)), 1),
            Row(2, Vectors.dense(Array(10.0, 20.0)), 0)
          )
        ),
        StructType(idFeaturesLabelScheme)
      )

      When("when I check for unique ids")

      val check = Transformations.isUniqueId(uniqueIdDF, "idn")

      Then("the check is true")
      assert(check === true)
    }

    scenario("dataframe with no unique ids") {
      Given("a dataframe with no unique id defined")

      val noUniqueIdDF = spark.createDataFrame(
        spark.sparkContext.parallelize(
          Seq(
            Row(2, Vectors.dense(Array(1.0, 2.0)), 1),
            Row(2, Vectors.dense(Array(10.0, 20.0)), 0)
          )
        ),
        StructType(idFeaturesLabelScheme)
      )

      When("when I check for unique ids")

      val check = Transformations.isUniqueId(noUniqueIdDF, "idn")

      Then("the check is true")
      assert(check === false)
    }
  }

  feature("maping values of labels") {

    scenario("a dataframe with values in the labels and a Map") {

      Given("some parameters")

      val filters = "feature4<4"
      val numP = 2
      val colLabel = "label"
      val labelsMap = Map(1 -> 10, 0 -> 20)
      val colId = "id"

      When(message = "the transformation is called")
      import spark.implicits._
      val dataInput = Seq(
        (1, 23, 3.0, 3.0, 10.0, "id1", 0),
        (2, 43, 4.0, 5.0, 50.0, "id2", 1),
        (2, 43, 4.0, 2.0, 50.0, "id1", 1)
      ).toDF(
        "feature1",
        "feature2",
        "feature3",
        "feature4",
        "feature5",
        colId,
        colLabel
      )

      val dfTransformed = Transformations.getDfWithFormat(
        Source.fromString("feature1,feature2,feature3,feature5"),
        dataInput,
        filters,
        numP,
        colLabel,
        labelsMap,
        colId,
        spark,
        Logger.getRootLogger()
      )

      Then(
        "the transformed dataframe have to have less elements that the original "
      )
      assert(dataWithFeatureCols.count > dfTransformed.count)

      And("the new dataframe have to have the columns with the format ")
      assert(Transformations.hasColumn(dfTransformed, Constants.COL_FEATURES))
      assert(Transformations.hasColumn(dfTransformed, Constants.COL_ID))
      assert(Transformations.hasColumn(dfTransformed, Constants.COL_LABEL))

      And(
        "the column features have to have the same elements that the file of test"
      )
      val firstFeatureVector =
        dfTransformed.select(Constants.COL_FEATURES).first()
      val firstArrayFeauture =
        firstFeatureVector.getAs[org.apache.spark.ml.linalg.Vector](0).toArray
      assert(firstArrayFeauture.length === 4)

      And("the labels have to have the correct mapped values")
      val transformedLabels =
        dfTransformed.select(Constants.COL_LABEL).distinct().collect()
      // the order cannot be ensure, so the test is used the sum of the lables
      assert(
        transformedLabels(0).getInt(0) == 20.0
          && transformedLabels(1).getInt(0) == 10.0
      )

      And("the id of the dataframe are unique")

      assert(
        Transformations.isUniqueId(dfTransformed, Constants.COL_ID) === true
      )

    }
  }

  feature("get Path for datasets on GCP") {
    scenario("a path in GCS") {

      Given("a path on gs")

      val path = "gs://dataproc-udea/output/dataset/pageblocks/"
      val path2 = "gs://dataproc-udea/output/dataset/pageblocks/_1"

      When("getOutPutForGCS is called")
      val result = Transformations.getOutPutForGCS(path, 1.toString)
      val result2 = Transformations.getOutPutForGCS(path2, 1.toString)
      Then("a path to save is computed")

      assert(result === "gs://dataproc-udea/output/dataset/pageblocks_1/")

    }
  }

}
