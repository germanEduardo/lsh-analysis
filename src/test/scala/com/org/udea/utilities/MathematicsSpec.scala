package com.org.udea.utilities

import com.org.udea.SparkTestWrapper
import org.apache.log4j.Logger
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}
import org.apache.spark.ml.linalg.{Vector, Vectors}
import org.apache.spark.sql.Row


class MathematicsSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter with SparkTestWrapper{

  feature("Dot product") {

    scenario("vectors in 2D") {

      Given("some vectors")
      val x : Vector = Vectors.dense(Array(1.0, 2.0))
      val y : Vector = Vectors.dense(Array(3.0, 2.0))
      val x_ortogonal : Vector = Vectors.dense(Array(1.0, 1.0))
      val y_ortogonal : Vector = Vectors.dense(Array(-1.0, 1.0))

      When("dot product is called  vectors")
      val result: Double = Mathematics.dot(x,y)
      val result_zero: Double = Mathematics.dot(x_ortogonal,y_ortogonal)

      Then("the result have to be sum")
      assert(result === 1.0*3.0+2.0*2.0)

      And("between ortogonals have to be zero")
      assert(result_zero === 0.0)
    }
  }

  feature("functions related with signatures") {

    scenario("arrays of 4 units to decimal") {

      Given("the zero, one and fiteen binary arrays")
      val zero : Array[Int] = Array(0,0,0,0)
      val one : Array[Int] = Array(0,0,0,1)
      val fifteen : Array[Int] = Array(1,1,1,1)

      When("is called the function binaryToDec")
      val zero_dec: Int = Mathematics.binaryToDec(zero)
      val one_dec: Int = Mathematics.binaryToDec(one)
      val fifteen_dec: Int = Mathematics.binaryToDec(fifteen)

      Then("the result have to 0")
      assert(zero_dec === 0)

      And("the result have to 1")
      assert(one_dec === 1)

      And("the result have to 15")
      assert(fifteen_dec === 15)
    }

    scenario("generate string signature from binary array") {

      Given("some binary array")
      val fifteen : Array[Int] = Array(1,1,1,1)

      When("is called the function binaryToDec")
      val signature: String = Mathematics.stringSignature(fifteen)

      Then("the result have to same array in strings")
      assert(signature === "1111")


    }
  }

  feature("centroid of a vector and ") {

    scenario("sparse vector and dense vector") {
      Given("a row with sparse/dense vector")

      When("is called vectorCentroid")
      val centroid = Mathematics.vectorCentroid(dummyUnevenSparse)
      val centroidDense = Mathematics.vectorCentroid(dummyUneven)

      Then("the centroid is computed")
      assert(centroid == Vectors.dense(Array(0.6000000000000001, 0.625)))
      assert(centroidDense == Vectors.dense(Array(0.6000000000000001, 0.625)))

    }

    scenario("wrong vector in row") {
      Given("a row with non sparse/dense vector")
      val nonRowSparseVectorRow = Seq(Row(1, Array(1.0, 2.0), 1))
      When("is called vectorCentroid")
      Then("an exception is raised")
      assertThrows[java.lang.ClassCastException] {
        Mathematics.vectorCentroid(nonRowSparseVectorRow)
      }


    }


  }


}
