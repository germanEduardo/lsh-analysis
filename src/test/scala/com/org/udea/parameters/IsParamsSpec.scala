package com.org.udea.parameters

import com.org.udea.SparkTestWrapper
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class IsParamsSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  feature("creation of object") {

    scenario("valid arguments") {

      Given(" the correct parameters ")

      val instances = someDF
      val unbalanced = true
      val minority = 1
      val neighbors = 3
      val subBuckets = 1
      val distancesIntervale = 500


      When(message = "the object isParams is created and the parameters unpacked")
      val params = new IsParams(instances, unbalanced, minority, spark, neighbors, subBuckets, distancesIntervale)

      Then("the tuples must to be equal")
      assert(params.unpackParams()===(instances,unbalanced,minority, spark, neighbors,subBuckets,distancesIntervale))
    }

  }





}
