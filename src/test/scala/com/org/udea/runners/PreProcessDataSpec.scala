package com.org.udea.runners

import com.org.udea.SparkTestWrapper
import com.org.udea.utilities.{Constants, Transformations}
import org.apache.spark.sql.SaveMode
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class PreProcessDataSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper {

  feature("run pre process of data") {

    dataWithFeatureCols.write
      .mode(SaveMode.Overwrite)
      .saveAsTable("testwithcols")

    spark.catalog.refreshTable("testwithcols")

    val fileBase = "src/test/resources/baseTest.txt"
    val filters = "feature4<4"
    val output = "testpreprocess"

    scenario("pre processing table ") {

      Given("some arguments")
      val args = Array(
        "-I",
        "testwithcols",
        "-p",
        "2",
        "-F",
        fileBase,
        "-f",
        filters,
        "-L",
        "0=10,1=20",
        "-o",
        output,
        "-l",
        "label",
        "-i",
        "id"
      )

      When("the preProcess data is Run")
      PreProcessData.main(args)
      val dfTransformed = spark.table(output)
      val dfOriginal = spark.table("testwithcols")

      Then("the table written have to be smaller than the original")
      assert(dfOriginal.count > dfTransformed.count)

      And("the new dataframe have to have the columns with the format ")
      assert(Transformations.hasColumn(dfTransformed, Constants.COL_FEATURES))
      assert(Transformations.hasColumn(dfTransformed, Constants.COL_ID))
      assert(Transformations.hasColumn(dfTransformed, Constants.COL_LABEL))

      And(
        "the column features have to have the same elements that the file of test"
      )
      val firstFeatureVector =
        dfTransformed.select(Constants.COL_FEATURES).first()
      val firstArrayFeauture =
        firstFeatureVector.getAs[org.apache.spark.ml.linalg.Vector](0).toArray
      assert(firstArrayFeauture.length === 4)

      And("the labels have to have the correct mapped values")
      val transformedLabels =
        dfTransformed.select(Constants.COL_LABEL).distinct().collect()
      // the order cannot be ensure, so the test is used the sum of the lables
      assert(
        transformedLabels(0).getInt(0) + transformedLabels(1).getInt(0) == 30.0
      )

    }

  }

}
