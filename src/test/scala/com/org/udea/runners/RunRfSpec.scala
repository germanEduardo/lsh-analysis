package com.org.udea.runners

import java.io.File

import com.org.udea.SparkTestWrapper
import org.apache.spark.sql.SaveMode
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RunRfSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper {

  val file_report = "reports/rf_report.csv"
  val numOfcols = 17

  before {
    test_balancedDF.write.mode(SaveMode.Overwrite).saveAsTable("test")

    new File(file_report).delete()

  }

  feature("run experiment") {

    scenario("with combined RF") {

      val json = "src/test/resources/noCatIndex.json"

      Given("some arguments")
      val args = Array(
        "-I",
        "test",
        "-p",
        "4",
        "-J",
        json,
        "-L",
        "1,0",
        "-o",
        "rf_report",
        "-C",
        "0",
        "-k",
        "2",
        "-S",
        "1",
        "-t",
        "2,5",
        "-d",
        "2,3",
        "-B",
        "0"
      )

      When("the experiment is Run")

      RunRf.main(args)

      val reportSaved = readFile(file_report)

      val numOfrows = 17

      Then("the result have to be " + numOfrows.toString + " rows: ")
      assert(reportSaved.length === numOfrows)
      And("the result have to be " + numOfcols.toString + " cols: ")
      assert(reportSaved(0).split(",").length === numOfcols)

    }

  }

}
