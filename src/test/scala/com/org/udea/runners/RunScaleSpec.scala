package com.org.udea.runners

import java.io.File
import com.org.udea.SparkTestWrapper
import org.apache.spark.sql.SaveMode
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RunScaleSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter with SparkTestWrapper {

    val file_report = "reports/scale_experiments.csv"
    val numOfcols = 18

    before {
        test_balancedDF.write.mode(SaveMode.Overwrite).
                saveAsTable("test")

        new File(file_report).delete()
    }

    feature("run experiment") {

        scenario("all the parameters") {

            Given("some arguments")
            val args = Array("-I" , "test" , "-p" , "4",
                "-L",  "1,0",  "-o", "scale_experiments",
                "-A",  "2,4",  "-O",  "1", "-B", "0",
                "-l",  "hyperplanes,projection,hyperplanes-projection",
                "-z",  "entropy,drop3",  "-s",  "0.1",
                "-n",  "4",  "-m",  "1000",  "-D",  "6",
                "-c", "boundaries",
                "-r", "0.5,1.0")

            When("the experiment is Run")

            RunScale.main(args)

            val reportSaved =  readFile(file_report)

            val numOfrows = 121

            Then("the result have to be " +  numOfrows.toString + " rows: ")
            assert(reportSaved.length === numOfrows)
            And("the result have to be "  + numOfcols.toString+ " cols: ")
            assert(reportSaved(0).split(",").length === numOfcols)

        }



    }



}
