package com.org.udea.runners

import com.org.udea.SparkTestWrapper
import com.org.udea.runners.ExampleDatasets.getGridConf
import com.org.udea.utilities.Constants
import org.apache.log4j.Logger
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}
import scala.util.Random

class ExampleDatasetsSpec extends FeatureSpec with GivenWhenThen with BeforeAndAfter with SparkTestWrapper {


  feature("get the cases to run") {

    scenario("all the parameters are given") {

      Given("Lists with the values to get combinations")
      val tableNames = Seq("dummy_data_v1", "dummy_data_v2")
      val lshs = Seq(Constants.LSH_HYPERPLANES_METHOD, Constants.LSH_PROJECTION_METHOD,
        Constants.LSH_COMBINED_METHOD)
      val ands = Seq(2, 4, 8)
      val isMethods = Seq(Constants.INSTANCE_SELECTION_ENTROPY_METHOD, Constants.INSTANCE_SELECTION_DROP3_METHOD)
      val oneClasses = Seq("one", "boundaries")


      When("the function is called")

      val gridConf = getGridConf(tableNames, lshs, ands, isMethods, oneClasses)

      Logger.getRootLogger().info(gridConf.length)

      Then("the gid have the length requested")
      assert(gridConf.length === 72)
      Then("the positions of a random element in  grid returned are in the correct position")
      val random = new Random
      val randomElement = gridConf(random.nextInt(gridConf.length))
      assert(tableNames.contains(randomElement._1))
      assert(lshs.contains(randomElement._2))
      assert(ands.contains(randomElement._3))
      assert(isMethods.contains(randomElement._4))
      assert(oneClasses.contains(randomElement._5))

    }


  }

}
