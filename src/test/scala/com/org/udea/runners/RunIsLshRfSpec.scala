package com.org.udea.runners

import java.io.File

import com.org.udea.SparkTestWrapper
import org.apache.spark.sql.SaveMode
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RunIsLshRfSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper {

  val file_report = "reports/testEntropyDrop3.csv"
  val file_reportNoModeldrop3 = "reports/testDrop3NoIsModel.csv"
  val fileReportNoModel = "reports/testEntropyNoIsModel.csv"
  val numOfcols = 33

  before {
    test_balancedDF.write.mode(SaveMode.Overwrite).saveAsTable("test")

    new File(file_report).delete()
    new File(file_reportNoModeldrop3).delete()
    new File(fileReportNoModel).delete()

  }

  feature("run experiment") {

    scenario("with combined LSH and drop3, entropy") {

      val json = "src/test/resources/noCatIndex.json"

      Given("some arguments")
      val args = Array(
        "-I",
        "test",
        "-p",
        "4",
        "-J",
        json,
        "-L",
        "1,0",
        "-o",
        "testEntropyDrop3",
        "-C",
        "0",
        "-k",
        "2",
        "-S",
        "1",
        "-t",
        "2",
        "-d",
        "2",
        "-A",
        "1,2",
        "-O",
        "1",
        "-B",
        "1",
        "-l",
        "hyperplanes-projection",
        "-z",
        "entropy,drop3",
        "-s",
        "0.1",
        "-n",
        "4",
        "-m",
        "1000",
        "-D",
        "6"
      )

      When("the experiment is Run")

      RunIsLshRf.main(args)

      val reportSaved = readFile(file_report)

      val numOfrows = 34

      Then("the result have to be " + numOfrows.toString + " rows: ")
      reportSaved.length === numOfrows
      And("the result have to be " + numOfcols.toString + " cols: ")
      reportSaved(0).split(",").length === numOfcols

    }

    scenario("with combined LSH and drop3 and without Model no IS") {

      val json = "src/test/resources/noCatIndex.json"

      Given("some arguments and instance selection")
      val args = Array(
        "-I",
        "test",
        "-p",
        "4",
        "-J",
        json,
        "-L",
        "1,0",
        "-o",
        "testDrop3NoIsModel",
        "-C",
        "0",
        "-k",
        "2",
        "-S",
        "1",
        "-t",
        "2",
        "-d",
        "2",
        "-A",
        "1,2",
        "-O",
        "1",
        "-B",
        "1",
        "-l",
        "hyperplanes-projection",
        "-z",
        "drop3",
        "-s",
        "0.1",
        "-n",
        "4",
        "-m",
        "1000",
        "-D",
        "6",
        "-N",
        "0",
        "-c",
        "boundaries"
      )

      When("the experiment is Run")
      RunIsLshRf.main(args)

      val report_saved = readFile(file_reportNoModeldrop3)

      val numOfrows = 9

      Then("the result have to be " + numOfrows.toString + " rows: ")
      report_saved.length === numOfrows
      And("the result have to be " + numOfcols.toString + " cols: ")
      report_saved(0).split(",").length === numOfcols

    }

    scenario("with combined LSH and entropy and without Model no IS") {

      val json = "src/test/resources/noCatIndex.json"

      Given("some arguments")
      val args = Array(
        "-I",
        "test",
        "-p",
        "4",
        "-J",
        json,
        "-L",
        "1,0",
        "-o",
        "testEntropyNoIsModel",
        "-C",
        "0",
        "-k",
        "2",
        "-S",
        "1",
        "-t",
        "2",
        "-d",
        "2",
        "-A",
        "1,2",
        "-O",
        "1",
        "-B",
        "1",
        "-l",
        "hyperplanes-projection",
        "-z",
        "entropy",
        "-s",
        "0.1",
        "-n",
        "4",
        "-m",
        "1000",
        "-D",
        "6",
        "-N",
        "0"
      )

      When("the experiment is Run")

      RunIsLshRf.main(args)

      val report_saved = readFile(fileReportNoModel)

      val numOfrows = 9

      Then("the result have to be " + numOfrows.toString + " rows: ")
      report_saved.length === numOfrows
      And("the result have to be " + numOfcols.toString + " cols: ")
      report_saved(0).split(",").length === numOfcols

    }

  }

}
