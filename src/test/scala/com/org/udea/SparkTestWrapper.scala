package com.org.udea

import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.linalg.SQLDataTypes.VectorType
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Row, SparkSession}

import java.io.File
import scala.io.Source
import scala.reflect.io.Directory

trait SparkTestWrapper {

  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val dirs = Seq("./spark-warehouse", "timeTest_lsh", "drop3_subucket")
  dirs.foreach(d => {
    val directory = new Directory(new File(d))
    directory.deleteRecursively()
  })

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("lsh-analysis-test ")
      .getOrCreate()
  }

  import spark.implicits._

  def round2dec(number: Double): Double = "%.2f".format(number).toDouble

  def readFile(file: String): Array[String] = {
    val bufferedSource = Source.fromFile(file)
    val result = bufferedSource.getLines.toArray
    bufferedSource.close
    return (result)

  }

  val idFeaturesLabelScheme = List(
    StructField("idn", IntegerType, true),
    StructField("features", VectorType, true),
    StructField("label", IntegerType, true)
  )

  val signSchema = List(
    StructField("idn", IntegerType, true),
    StructField("features", VectorType, true),
    StructField("label", IntegerType, true),
    StructField("signature", IntegerType, true)
  )

  val someSchemaWithSign = List(
    StructField("idn", IntegerType, true),
    StructField("features", VectorType, true),
    StructField("label", IntegerType, true),
    StructField("signature", org.apache.spark.sql.types.StringType, true)
  )

  val someData = Seq(
    Row(1, Vectors.dense(Array(1.0, 2.0)), 1),
    Row(2, Vectors.dense(Array(10.0, 20.0)), 0),
    Row(3, Vectors.dense(Array(100.0, 200.0)), 1),
    Row(4, Vectors.dense(Array(100.0, 200.0)), 1)
  )

  val someDF = spark.createDataFrame(
    spark.sparkContext.parallelize(someData),
    StructType(idFeaturesLabelScheme)
  )

  val oneClassData = Seq(
    Row(1, Vectors.dense(Array(1.0, 2.0)), 1, "1"),
    Row(2, Vectors.dense(Array(10.0, 20.0)), 1, "1"),
    Row(3, Vectors.dense(Array(1.0, 2.0)), 1, "1"),
    Row(4, Vectors.dense(Array(10.0, 20.0)), 1, "1")
  )

  val oneClassDF = spark.createDataFrame(
    spark.sparkContext.parallelize(oneClassData),
    StructType(someSchemaWithSign)
  )

  val unBalancedData_one = Seq(
    Row(1, Vectors.dense(Array(1.0, 2.0)), 1),
    Row(2, Vectors.dense(Array(10.0, 20.0)), 0),
    Row(3, Vectors.dense(Array(100.0, 200.0)), 1),
    Row(4, Vectors.dense(Array(1.0, 2.0)), 0),
    Row(5, Vectors.dense(Array(10.0, 20.0)), 0),
    Row(6, Vectors.dense(Array(100.0, 200.0)), 0)
  )

  val unBalancedDF_one = spark.createDataFrame(
    spark.sparkContext.parallelize(unBalancedData_one),
    StructType(idFeaturesLabelScheme)
  )

  val unBalancedData_zero = Seq(
    Row(1, Vectors.dense(Array(1.0, 2.0)), 0),
    Row(2, Vectors.dense(Array(10.0, 20.0)), 0),
    Row(3, Vectors.dense(Array(100.0, 200.0)), 1),
    Row(4, Vectors.dense(Array(1.0, 2.0)), 1),
    Row(5, Vectors.dense(Array(10.0, 20.0)), 1),
    Row(6, Vectors.dense(Array(100.0, 200.0)), 1)
  )

  val unBalancedDF_zero = spark.createDataFrame(
    spark.sparkContext.parallelize(unBalancedData_zero),
    StructType(idFeaturesLabelScheme)
  )

  val dataForTest_unbalanced = Seq(
    Row(1, Vectors.dense(Array(1.0, 0.0)), 0),
    Row(2, Vectors.dense(Array(-1.0, 0.0)), 0),
    Row(3, Vectors.dense(Array(0.5, 0.0)), 0),
    Row(4, Vectors.dense(Array(-0.5, 0.0)), 0),
    Row(5, Vectors.dense(Array(0.5, 0.5)), 0),
    Row(6, Vectors.dense(Array(-0.5, 0.5)), 0),
    Row(7, Vectors.dense(Array(0.5, -0.5)), 0),
    Row(8, Vectors.dense(Array(-0.5, -0.5)), 1),
    Row(9, Vectors.dense(Array(1.0, 1.0)), 1),
    Row(10, Vectors.dense(Array(-1.0, 1.0)), 0),
    Row(11, Vectors.dense(Array(1.0, -1.0)), 0),
    Row(12, Vectors.dense(Array(-1.0, -1.0)), 0),
    Row(13, Vectors.dense(Array(0.0, 1.0)), 0),
    Row(14, Vectors.dense(Array(0.0, -1.0)), 1)
  )

  val test_unbalancedDF = spark.createDataFrame(
    spark.sparkContext.parallelize(dataForTest_unbalanced),
    StructType(idFeaturesLabelScheme)
  )

  val dataForTest_balanced = Seq(
    Row(1, Vectors.dense(Array(1.0, 0.0)), 0),
    Row(2, Vectors.dense(Array(-1.0, 0.0)), 1),
    Row(3, Vectors.dense(Array(0.5, 0.0)), 0),
    Row(4, Vectors.dense(Array(-0.5, 0.0)), 1),
    Row(5, Vectors.dense(Array(0.5, 0.5)), 1),
    Row(6, Vectors.dense(Array(-0.5, 0.5)), 0),
    Row(7, Vectors.dense(Array(0.5, -0.5)), 1),
    Row(8, Vectors.dense(Array(-0.5, -0.5)), 0),
    Row(9, Vectors.dense(Array(1.0, 1.0)), 1),
    Row(10, Vectors.dense(Array(-1.0, 1.0)), 0),
    Row(11, Vectors.dense(Array(1.0, -1.0)), 1),
    Row(12, Vectors.dense(Array(-1.0, -1.0)), 0),
    Row(13, Vectors.dense(Array(0.0, 1.0)), 1),
    Row(14, Vectors.dense(Array(0.0, -1.0)), 0)
  )

  val test_balancedDF = spark.createDataFrame(
    spark.sparkContext.parallelize(dataForTest_balanced),
    StructType(idFeaturesLabelScheme)
  )

  val dataToTrain = Seq(
    Row(1, Vectors.dense(Array(1.0, 1.0, 10.0)), 0),
    Row(2, Vectors.dense(Array(0.0, 2.0, 20.0)), 1),
    Row(3, Vectors.dense(Array(0.0, 3.0, 10.0)), 0),
    Row(4, Vectors.dense(Array(1.0, 4.0, 10.0)), 1),
    Row(5, Vectors.dense(Array(1.0, 5.0, 10.0)), 1)
  )

  val dataToTrainDF = spark.createDataFrame(
    spark.sparkContext.parallelize(dataToTrain),
    StructType(idFeaturesLabelScheme)
  )

  val dataToTransform = Seq(
    Row(1, Vectors.dense(Array(1.0, 1.0, 10.0, 1.0)), 0),
    Row(2, Vectors.dense(Array(0.0, 2.0, 20.0, 2.0)), 1),
    Row(3, Vectors.dense(Array(0.0, 3.0, 10.0, 4.0)), 0),
    Row(4, Vectors.dense(Array(1.0, 4.0, 10.0, 8.0)), 1),
    Row(5, Vectors.dense(Array(1.0, 5.0, 10.0, 16.0)), 1)
  )

  val dataToTransformDF = spark.createDataFrame(
    spark.sparkContext.parallelize(dataToTransform),
    StructType(idFeaturesLabelScheme)
  )

  val someDataWithFeatures = Seq(
    Row("1", 1.0, 1.0, 1.0, 1, 10, 1),
    Row("2", 2.0, 2.0, 2.0, 2, 20, 1),
    Row("3", 3.0, 3.0, 3.0, 3, 30, 0),
    Row("4", 4.0, 4.0, 4.0, 4, 40, 0)
  )

  val featuresSchema = List(
    StructField("id", org.apache.spark.sql.types.StringType, true),
    StructField("feature1", DoubleType, true),
    StructField("feature2", DoubleType, true),
    StructField("feature3", DoubleType, true),
    StructField("feature4", IntegerType, true),
    StructField("feature5", IntegerType, true),
    StructField("label", IntegerType, true)
  )

  val dataWithFeatureCols = spark.createDataFrame(
    spark.sparkContext.parallelize(someDataWithFeatures),
    StructType(featuresSchema)
  )

  val someDataForSubBucket = Seq(
    Row(1, Vectors.dense(Array(1.0, 0.0)), 0, "1"),
    Row(2, Vectors.dense(Array(-1.0, 0.0)), 1, "1"),
    Row(3, Vectors.dense(Array(0.5, 0.0)), 0, "2"),
    Row(4, Vectors.dense(Array(-0.5, 0.0)), 1, "2"),
    Row(5, Vectors.dense(Array(0.5, 0.5)), 1, "2"),
    Row(6, Vectors.dense(Array(-0.5, 0.5)), 0, "2"),
    Row(7, Vectors.dense(Array(0.5, -0.5)), 1, "2"),
    Row(8, Vectors.dense(Array(-0.5, -0.5)), 0, "2"),
    Row(9, Vectors.dense(Array(1.0, 1.0)), 1, "3"),
    Row(10, Vectors.dense(Array(-1.0, 1.0)), 0, "3"),
    Row(11, Vectors.dense(Array(1.0, -1.0)), 1, "3"),
    Row(12, Vectors.dense(Array(-1.0, -1.0)), 0, "3"),
    Row(13, Vectors.dense(Array(0.0, 1.0)), 1, "3"),
    Row(14, Vectors.dense(Array(0.0, 1.0)), 1, "3"),
    Row(15, Vectors.dense(Array(0.0, -1.0)), 0, "5")
  )

  val dummyInfo = Seq(
    Row(Vectors.dense(Array(0.0, 0.0)), 1, 1),
    Row(Vectors.dense(Array(0.0, 1.0)), 2, 0),
    Row(Vectors.dense(Array(1.0, 0.0)), 3, 1),
    Row(Vectors.dense(Array(1.0, 1.0)), 4, 1)
  )

  val dummyUneven = Seq(
    Row(Vectors.dense(Array(0.3, 0.3)), 1, 1),
    Row(Vectors.dense(Array(0.5, 0.5)), 2, 1),
    Row(Vectors.dense(Array(0.4, 0.6)), 3, 1),
    Row(Vectors.dense(Array(1.2, 1.1)), 4, 1)
  )

  val minorClassData = Seq(
    Row(Vectors.dense(Array(0.3, 0.3)), 1, 0),
    Row(Vectors.dense(Array(0.5, 0.5)), 2, 0),
    Row(Vectors.dense(Array(0.4, 0.6)), 3, 0),
    Row(Vectors.dense(Array(1.2, 1.1)), 4, 0)
  )

  val idx1 = Array(0, 1, 10, 50, 60, 65, 78, 80, 81, 82, 83, 100, 120, 121, 122,
    125, 127, 129, 130)
  val val1 = Array(1.0, 0.1, 200.0, 300.0, 50.0, 1000000.0, 1.0, 1.0, 2.0, 3.0,
    4.0, 5.0, 1.0, 0.5, 0.75, 0.8, 0.9, 2.5, 3.3)
  val val2 = Array(0.0002, 0.1, 200, 300.0, 50.0, 1000000.0, 1.0, 1.0, 2.0, 0.1,
    4.0, 5.0, 1.0, 0.5, 0.75, 0.8, 0.9, 2.5, 1.0)
  val idx2 = Array(10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 130)
  val val3 = Array(1.0, 10.0, 100.0, 1000.0, 20.0, 300.0, 4000.0, 50000.0, 0.1,
    0.0002, 1000000)
  val val4 = Array(1.0, 0.1, 0.2, 0.3, 0.4, 0.5, 1000000.0, 3000000000.0,
    50000000000.0, 8000000000000.0, 90000000000000.0)
  val largeVectorInstances = Seq(
    Row(Vectors.sparse(131, idx1, val1), 1, 0),
    Row(Vectors.sparse(131, idx1, val2), 2, 0),
    Row(Vectors.sparse(131, idx2, val3), 3, 0),
    Row(Vectors.sparse(131, idx2, val4), 4, 0),
    Row(Vectors.sparse(131, idx1, val1), 5, 0)
  )

  val largeVectorInstances2 = Seq(
    Row(Vectors.sparse(131, idx1, val1), 1, 1),
    Row(Vectors.sparse(131, idx1, val2), 2, 0),
    Row(Vectors.sparse(131, idx2, val3), 3, 0),
    Row(Vectors.sparse(131, idx2, val4), 4, 1),
    Row(Vectors.sparse(131, idx1, val1), 5, 1)
  )

  val dummyUnevenSparse = Seq(
    Row(Vectors.sparse(2, Array(0, 1), Array(0.3, 0.3)), 1, 1),
    Row(Vectors.sparse(2, Array(0, 1), Array(0.5, 0.5)), 2, 1),
    Row(Vectors.sparse(2, Array(0, 1), Array(0.4, 0.6)), 3, 1),
    Row(Vectors.sparse(2, Array(0, 1), Array(1.2, 1.1)), 4, 1)
  )

  val dummyData = Seq(
    Row(1, Vectors.dense(Array(0.0, 0.0)), 1, "1"),
    Row(2, Vectors.dense(Array(0.0, 1.0)), 0, "1"),
    Row(3, Vectors.dense(Array(1.0, 0.0)), 1, "1"),
    Row(4, Vectors.dense(Array(1.0, 1.0)), 1, "1")
  )

  val dummy = spark.createDataFrame(
    spark.sparkContext.parallelize(dummyData),
    StructType(someSchemaWithSign)
  )

  val dfForTestSubBucket = spark.createDataFrame(
    spark.sparkContext.parallelize(someDataForSubBucket),
    StructType(someSchemaWithSign)
  )

// create dataset

  val dataForDrop3Testing = Seq(
    Row(1, Vectors.dense(Array(1.0, 0.0)), 0, "1"),
    Row(3, Vectors.dense(Array(0.5, 0.0)), 1, "1")
  )

  val dfForDrop3Testing = spark.createDataFrame(
    spark.sparkContext.parallelize(dataForDrop3Testing),
    StructType(someSchemaWithSign)
  )

}
