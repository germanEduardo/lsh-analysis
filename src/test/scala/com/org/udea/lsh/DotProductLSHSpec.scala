package com.org.udea.lsh

import com.org.udea.SparkTestWrapper
import com.org.udea.utilities.{Constants}
import org.apache.log4j.Logger
import org.apache.spark.ml.linalg.{Vectors}
import org.apache.spark.sql.{Row}
import org.apache.spark.sql.types.{StructType}
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class DotProductLSHSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  feature("creation of object") {

    scenario("invalid arguments") {

      Given("an invalid number of hash tables in And on Or functions")

      val andsFunctions_zero = 0
      val orsFunctions = 3

      val andsFunctions= 1
      val orsFunctions_zero = 0


      When(message = "the object hyperplanes is created")

      Then("IllegalArgumentException should trigger when ands are 0")
      assertThrows[IllegalArgumentException] {
        val hyp = new DotProductLSH(someDF, andsFunctions_zero, orsFunctions, 3 , spark)
      }

      And("IllegalArgumentException should trigger when or are 0")
      assertThrows[IllegalArgumentException] {
        val hyp = new DotProductLSH(someDF, andsFunctions, orsFunctions_zero, 3,  spark)
      }

    }

    scenario("valid Arguments") {

      Given("valid number of hash tables")

      val andsFunctions = 2
      val orsFunctions = 2

      When(message = "the object hyperplanes is created and hyperplanes created")

      val rp = new DotProductLSH(someDF, andsFunctions, orsFunctions, 3, spark)
      val functionFamilies = rp.getFamilies

      Then("the number of features of or functions")

      assert(functionFamilies.size == orsFunctions)

      And("the number of features of ands functions")

      assert(functionFamilies(0).size == andsFunctions)

    }

    scenario("hash functions") {

      Given("a Family of functions, an instance and OR-Ands functions" )

      val familieFunctions = List((Vectors.dense(0.1, 0.2), 1.0),
        (Vectors.dense(0.3, 0.002), 2.0),
        (Vectors.dense(0.8, 0.78), 0.67))

      val instance = Vectors.dense(23,5 )
      val andsFunctions = 2
      val orsFunctions = 2
      val rp = new DotProductLSH(someDF, andsFunctions, orsFunctions, 3, spark)

      When("when I hash the x vector with famile of functions defined")

      val signature = rp.hashFunction(instance, "1", familieFunctions)

      Then("The Signature have to be 1127")

      assert(signature === "1127")

    }

    scenario("signatures generation"){

      Given("a Dataframe with the correct structure and a Famile functions," +
        " OR-AND functions and instaced object")

      val dataToTest = Seq(
        Row(1, Vectors.dense(Array(3.0, 0.5)), 0),
        Row(2, Vectors.dense(Array(4.0, 0.4)), 0),
        Row(3, Vectors.dense(Array(-0.5, 3.0)), 1),
        Row(4, Vectors.dense(Array(-0.4, 4.0)), 0),
        Row(5, Vectors.dense(Array(-0.5, -3.0)), 1),
        Row(6, Vectors.dense(Array(-0.4, -4.0)), 0)
      )

      val dataframeToTest = spark.createDataFrame(
        spark.sparkContext.parallelize(dataToTest),
        StructType(idFeaturesLabelScheme))

      val familieFunctions = Seq(List((Vectors.dense(0.1, 0.2), 1.0),
        (Vectors.dense(0.3, 0.002), 2.0)),
        List((Vectors.dense(0.567, 0.89), 1.4),
          (Vectors.dense(- 0.22, 0.98), 2.9)),
        List((Vectors.dense(0.6637, 0.289), 1.4),
          (Vectors.dense(- 0.56, 0.198), 2.9))
      )

      val andsFunctions = 2
      val orsFunctions = 2

      val rp = DotProductLSH(dataframeToTest, andsFunctions, orsFunctions, 3, spark)

      When("I set  an Object RandomProjectionLSH with the famile fucntions defined")

      rp.setFamilies(familieFunctions)
      val df_with_signatures = rp.lsh(Constants.COL_FEATURES)

      Logger.getRootLogger().info("the dataframe is:")
      Logger.getRootLogger().info(df_with_signatures.show)

      val x: Array[String] = df_with_signatures.select(Constants.COL_SIGNATURE).distinct().collect.map(_.getString(0))

      Then("the signatures sholud be the followed")
      val expected= Array("000", "001", "110", "111", "112", "1-10", "1-1-1", "210", "201", "200", "2-10")
      assert(x.sorted === expected.sorted)

    }


  }

}
