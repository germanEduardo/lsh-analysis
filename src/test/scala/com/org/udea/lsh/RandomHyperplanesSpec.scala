package com.org.udea.lsh

import com.org.udea.SparkTestWrapper
import com.org.udea.utilities.{Constants}
import org.apache.log4j.Logger
import org.apache.spark.ml.linalg.{DenseVector, Vector, Vectors}
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RandomHyperplanesSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  feature("creation of object") {

    scenario("invalid arguments") {

      Given("an invalid number of hash tables")

      val numHashTablesFail = 0

      When(message = "the object hyperplanes is created")

      Then("IllegalArgumentException should trigger")
      assertThrows[IllegalArgumentException] {
        val hyp = new RandomHyperplanesLSH(someDF, numHashTablesFail, spark)
      }

    }

    scenario("valid Arguments") {

      Given("valid number of hash tables")
      val numHashTables = 4

      When(message = "the object hyperplanes is created and hyperplanes created")
      val hyp = new RandomHyperplanesLSH(someDF, numHashTables, spark)
      val hyperplanes: Array[Vector] = hyp.createHiperplanes()
      val y = someDF.select("features").head
      val numFeatures = y(0).asInstanceOf[DenseVector].size

      Then("correct number of planes is created")
      assert(numHashTables == hyperplanes.length)

      And("the number of features is corrected")
      assert(numFeatures == hyperplanes(0).size)

    }

    scenario("hash function") {

      Given("an vector X , hyperplanes defined and instanced object of Hyperplanes")

      val x = Vectors.dense(1.0, 0.5, 3.0)
      val h = Array(Vectors.dense(4.0, -4.0, 1.0),
        Vectors.dense(-4.0, -4.0, 1.0),
        Vectors.dense(6.0, -4.0, 1.0),
        Vectors.dense(4.0, -14.0, 1.0))
      val numHashTables = 4
      val hyp = new RandomHyperplanesLSH(someDF, numHashTables, spark)

      When("I hash the x vector with hyperplaes defined")

      val signature = hyp.hashFunction(x, h)

      Then("The Signature have to be 1011")

      assert(signature === "1011")

    }

    scenario("signatures generation with balanced dataframe") {

      Given("some hyperplanes")
      val numHashTables = 2
      val hyperplanes = Array(
        Vectors.dense(0, 1),
        Vectors.dense(1, 0))


      val hyp = RandomHyperplanesLSH(test_balancedDF, numHashTables, spark)
      hyp.setHyperplanes(hyperplanes)

      When("the signature are generated")
      val instancesWithSignature = hyp.lsh(Constants.COL_FEATURES)
      val keysDF = LshMethods.getKeys(instancesWithSignature)
      val keys: Array[String] = keysDF.select(Constants.COL_SIGNATURE).distinct().take(4).map(_.getString(0))

      Logger.getRootLogger().info("the dataframe is:")
      Logger.getRootLogger().info(instancesWithSignature.show)

      Then(" the size of keys should be 4")
      assert( keysDF.count === 4)

      And("the keys should be the expected")
      assert(keys.contains("11"))
      assert(keys.contains("01"))
      assert(keys.contains("00"))
      assert(keys.contains("10"))
    }


  }


}
