package com.org.udea.lsh

import com.org.udea.SparkTestWrapper
import com.org.udea.utilities.Constants
import org.apache.log4j.Logger
import org.apache.spark.ml.linalg.Vectors
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class RandomHyperplanesDotProductLSHSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  val ands = 1
  val ors = 1
  val w = 0.1
  val familieFunctions = List((Vectors.dense(1.0, 0.0), 1.0))
  val hyperplanes = Array(Vectors.dense(0, 1))

  feature("hashing") {

    scenario("instance examples") {

      Given("a isntance a object of RandomHyperplanesDotProductLSH")
      val instance = Vectors.dense(1.0, 1.0)
      val hash = new RandomHyperplanesDotProductLSH(test_balancedDF, ands, ors, w, spark)

      When("the instance is hashed")
      val sign = hash.hashFunction(instance, hyperplanes, "1", familieFunctions)

      Then("the signature have to be '1201'")
      assert(sign === "1201")

    }

  }


    feature("LSH") {

      scenario("dataframe sample") {

        Given("a dataframe")

        val hash = new RandomHyperplanesDotProductLSH(test_balancedDF, ands, ors, w, spark)
        hash.setFamilies(Seq(familieFunctions))
        hash.setHyperplanes(hyperplanes)

        When( "the instance is hashed")
        val signaturesDF = hash.lsh(Constants.COL_FEATURES)

        val keysDF = LshMethods.getKeys(signaturesDF)
        val keys = keysDF.select(Constants.COL_SIGNATURE).distinct().collect().
          map(_.getString(0))

        Logger.getRootLogger().info(signaturesDF.show)

        Then("the number of keys should be 10")
        assert(keysDF.count === 10.0)
        val expected = Array("0201", "0100", "0151",
          "0101", "051", "0150", "001", "0200", "000", "050")
        And("the keys have to be:  " + expected.mkString(","))
        assert(keys.sorted === expected.sorted)

      }

  }



}
