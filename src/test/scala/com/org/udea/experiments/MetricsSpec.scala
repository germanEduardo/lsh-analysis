package com.org.udea.experiments

import com.org.udea.SparkTestWrapper
import org.apache.log4j.Logger
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class MetricsSpec extends FeatureSpec with GivenWhenThen
  with BeforeAndAfter with SparkTestWrapper{


  feature("get metrics") {

    import spark.implicits._
    scenario("a datarame") {

      Given("a DataFrame")

      val dfPred = Seq((1, 1), (1, 0),
        (0, 0), (0, 1)).toDF("label", "prediction")

      val lb = Array(1, 0)

      When(message = "the metrics is instanced")
      val metrics = new Metrics[Int](dfPred, lb)

      Then("the metrics should be computed")
      assert(metrics.tp===1)
      assert(metrics.tn===1)
      assert(metrics.fn===1)
      assert(metrics.fp===1)

      And ("sensibility")
      assert(metrics.sensibility===50.0)
      And ("specificity")
      assert(metrics.specificity ===50.0)
      And ("precision")
      assert(metrics.precision ===50.0)
      And ("accuracy")
      assert(metrics.accuracy ===50.0)

      And("the other measures are computed")
      assert(metrics.getGeometricMean() === 50.0 )
      assert(metrics.getF1() === 50.0 )
      assert(metrics.getWeigthedAccuracy(0.7) === 50.0 )

      And("areaUnder ROC curve is computed")
      assert(metrics.areaUnderROC(spark)===0.5)

    }

  }

}
