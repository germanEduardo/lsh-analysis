package com.org.udea.experiments

import java.io.File

import com.github.mrpowers.spark.fast.tests.DatasetComparer
import com.org.udea.SparkTestWrapper
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class ReportSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper
    with DatasetComparer {

  val file = "temp/path.csv"

  before {

    new File(file).delete()

  }

  feature("creation of object report") {

    val factors = Array("IS", "trees", "depth", "time")
    val metrics = Array("tp", "accuaracy", "specificity", "sensibility")

    scenario("creation of object") {

      Given("factors and metrics names")

      When(message =
        "the report is created with default and with no default sep"
      )
      val report = new Report(factors, metrics)
      val reportSep = new Report(factors, metrics, "|")

      Then("the header should be created properly")
      assert(
        report.header === "IS,trees,depth,time,tp,accuaracy,specificity,sensibility"
      )

      And("the header with the custon sep shuould created")
      assert(
        reportSep.header === "IS|trees|depth|time|tp|accuaracy|specificity|sensibility"
      )

    }

    scenario("save the report") {
      Given("a report object and some values")
      val factorValues = Array("NO", "100", "30", "1")
      val metricsValues = Array(75.0, 50.0, 75.0, 100.0)

      val factorValuesBad = Array("NO", "100", "30")
      val metricsValuesBad = Array(75.0, 50.0, 75.0)

      val report = new Report(factors, metrics)
      When("the save function is called")
      report.saveReport(file, factorValues, metricsValues)

      Then("the file have to be saved with header if it the first")
      assert(readFile(file).length === 2)

      And("if the second, no header have to be created")
      report.saveReport(file, factorValues, metricsValues)
      assert(readFile(file).length === 3)

      And("when is try to save")
      assertThrows[IllegalArgumentException] {
        report.saveReport(file, factorValuesBad, metricsValuesBad)
      }

    }

  }

  feature("get the time of spark operation saving") {

    Given("a spark dataframe with transformations")

    val data = test_balancedDF

    When(message = "the function getDFandTime is called")
    val factors = Array("IS", "trees")
    val metrics = Array("tp", "accuaracy")
    val report = new Report(factors, metrics)
    val (dfResult, time) = report.getDFandTime(data, spark, "lsh")
    val dfSaved = spark.read.parquet("timeTest_" + "lsh")

    Then(
      "the time should be more than 0 and the daframe resulted have to be equal"
    )
    assertSmallDatasetEquality(data, dfSaved)
    assert(time > 0)

  }

}
