package com.org.udea.experiments

import java.io.File

import com.org.udea.SparkTestWrapper
import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.utilities.{Constants, Transformations}
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.RandomForestClassifier
import org.scalatest.{BeforeAndAfter, FeatureSpec, GivenWhenThen}

class ExperimentSpec
    extends FeatureSpec
    with GivenWhenThen
    with BeforeAndAfter
    with SparkTestWrapper {

  val file = "temp/report_exeperiment.csv"

  before {

    new File(file).delete()

  }

  feature("save results of an experiment") {

    scenario("an experiment already finished of LSH and IS") {

      val trees: Int = 2
      val depth: Int = 10
      val k: Int = 5
      val labels = Array(1, 0)
      val rf = new RandomForestClassifier()
        .setLabelCol(Constants.COL_LABEL)
        .setFeaturesCol(Constants.COL_INDEXED_FEATURES)
        .setNumTrees(trees)
        .setMaxDepth(depth)
        .setMaxMemoryInMB(512)
      val model = new Pipeline().setStages(Array(rf))
      val metadataFile = "src/test/resources/testIndex.json"
      val train = Transformations.dfManualIndex(dataToTrainDF, metadataFile)
      val test = Transformations.dfManualIndex(dataToTrainDF, metadataFile)

      Given("some values")

      val modelFitted = model.fit(train)
      val factors = Array(
        "instanceSelection",
        "lshMEthod",
        "sizeBucket",
        "ands",
        "ors",
        "imbalance",
        "neigh",
        "subBuckets",
        "distance",
        "isMethod",
        "trees",
        "depth",
        "type",
        "k"
      )
      val metrics = Array(
        "tp",
        "tn",
        "fp",
        "fn",
        "sensibility",
        "specificity",
        "precision",
        "accuracy",
        "F1",
        "Gmean",
        "wtdAcc",
        "AreaROC",
        "timeLSH",
        "timeIS",
        "buckets",
        "buckMax",
        "buckMin",
        "buckAvg",
        "reduction"
      )

      val report = new Report(factors, metrics, sep = "|")

      val modelArray = Array(trees.toString, depth.toString)

      val (
        timeLSH,
        timeIS,
        numeroDeCubetas,
        maxValue,
        minValue,
        avgValue,
        reduction
      ) =
        (1, 2, 100, 10, 50, 25, 0.75)

      val metricsIs = Array(
        timeLSH,
        timeIS,
        numeroDeCubetas,
        maxValue,
        minValue,
        avgValue,
        reduction
      )

      val (
        instanceSelection,
        lshMethod,
        sizeBucket,
        ands,
        ors,
        imbalance,
        neigh,
        subBuckets,
        distance,
        isMethod
      ) = ("NO", "lsh", "2", "2", "0", "true", "3", "1", "100", "entropy")

      val factorsVals = Array(
        instanceSelection,
        lshMethod,
        sizeBucket,
        ands,
        ors,
        imbalance,
        neigh,
        subBuckets,
        distance,
        isMethod
      )

      When("the save results is called")
      val ex = new ISLSHExperiment()
      val ls = new LshParams(lshMethod, sizeBucket.toInt, ands.toInt, ors.toInt)
      val is = new IsParams(
        imbalance.toBoolean,
        1,
        spark,
        neigh.toInt,
        subBuckets.toInt,
        distance.toInt
      )
      val runParams = new RunParams(k, labels, modelArray, file)

      ex.set(is, isMethod, ls, runParams, factors, metrics)
      ex.saveResults(
        modelFitted,
        train,
        test,
        report,
        factorsVals,
        metricsIs,
        runParams,
        spark
      )

      Then("the report have to be saved with the strcuture defined")
      assert(readFile(file).length === 3)

      //TODO: implement a test to truly evaluate tha report values

    }

  }

}
