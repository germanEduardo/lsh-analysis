package com.org.udea

import org.apache.spark.sql.SparkSession

trait SparkSessionWrapper extends Serializable {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .appName("lsh-analysis")
      .enableHiveSupport()
      .getOrCreate()
  }

}
