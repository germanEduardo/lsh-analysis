package com.org.udea.experiments

import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.utilities.{LogHelper, Transformations}
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.{Dataset, Row, SparkSession}

trait Experiment extends LogHelper {

  def set(
      isParams: IsParams,
      isMethod: String,
      lshParams: LshParams,
      runParams: RunParams,
      factors: Array[String],
      metrics: Array[String]
  )

  def run(
      trainingData: Dataset[Row],
      testingData: Dataset[Row],
      model: Pipeline,
      spark: SparkSession,
      metadataFile: String,
      nameAuxForTables: String,
      numberPartitions: Int,
      methodOneClass: String
  ): Unit

  def saveResults(
      model: PipelineModel,
      train: Dataset[Row],
      test: Dataset[Row],
      report: Report,
      factors: Array[String],
      metricsIS: Array[Double],
      runParams: RunParams,
      spark: SparkSession
  ): Unit = {

    val (k, labels, modelParams, path) = runParams.unpackParams()

    logger.info("using " + path)

    logger.info("training Metrics")
    var predictions = model.transform(train)
    val trainMetrics = new Metrics[Int](predictions, labels)
    val desMetricsTrain = trainMetrics.getDesbalancedMetrics(spark = spark)

    var metricsValues = trainMetrics.getBasics() ++ desMetricsTrain ++ metricsIS
    var factorsValues = factors ++ modelParams ++ Array("Train") ++
      Array(k.toString)
    report.saveReport(path, factorsValues, metricsValues)

    logger.info("testing Metrics")
    predictions = model.transform(test)
    val testMetric = new Metrics[Int](predictions, labels)
    val desMetricsTest = testMetric.getDesbalancedMetrics(spark = spark)
    metricsValues = testMetric.getBasics() ++ desMetricsTest ++ metricsIS
    factorsValues = factors ++ modelParams ++ Array("Test") ++
      Array(k.toString)
    report.saveReport(path, factorsValues, metricsValues)

  }

  def getTrainTestWithIndex(
      train: Dataset[Row],
      test: Dataset[Row],
      metadataFile: String
  ): (Dataset[Row], Dataset[Row]) = {

    val trainingData = Transformations.dfManualIndex(train, metadataFile)
    val testingData = Transformations.dfManualIndex(test, metadataFile)

    (trainingData, testingData)

  }

}
