package com.org.udea.experiments

import com.org.udea.instanceselection.InstanceSelection
import com.org.udea.lsh.LshMethods
import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.utilities.{LogHelper, Transformations}
import org.apache.spark.ml.Pipeline
import org.apache.spark.sql.{Dataset, Row, SparkSession}

class ScaleExperiment extends LogHelper  {

  var isParamsEx: IsParams = _
  var lshParamsEx: LshParams = _
  var factorsEx: Array[String] = _
  var metricsEx: Array[String] = _
  var ISMethodEx: String = _

  def saveResults(report: Report,factors: Array[String],
                  metricsIS: Array[Double],
                  reportPath: String,
                  spark: SparkSession): Unit = {


    logger.info("using " + reportPath)

    val factorsValues = factors
    val metricsValues = metricsIS
    report.saveReport(reportPath, factorsValues, metricsValues)


  }

  def set(isParams: IsParams, ISmethod: String,
          lshParams: LshParams,
          factors: Array[String], metrics: Array[String]) : Unit = {

    isParamsEx = isParams
    lshParamsEx = lshParams
    factorsEx = factors
    metricsEx = metrics
    ISMethodEx = ISmethod

  }

  def run(data: Dataset[Row], rate: Double,
          executors: Int, spark: SparkSession,
          nameAuxForTables: String,
          numberPartitions: Int,
          methodOneClass: String,
          reportPath: String): Unit = {

    val report = new Report(factorsEx, metricsEx)

    logger.info("performing Instance Selection")

    val (method, sizeBucket, andFunctions, orFunctions) = lshParamsEx.unpackParams()
    val data_sample = data.sample(rate)
    logger.info("using sample rate: " + rate.toString + " and the counting: " +  data_sample.count().toString)

    val (dfWithSignature, timeLSH) = report.getDFandTime(
      LshMethods.lsh(method, data_sample, spark,
        sizeBucket, andFunctions, orFunctions, numberPartitions), spark, "LSH_" + nameAuxForTables)

    val (numeroDeCubetas, maxValue, minValue, avgValue) = LshMethods.infoLSH(dfWithSignature)

    isParamsEx.setInstances(dfWithSignature)

    val (dfSampled, timeIS) = report.getDFandTime(
      InstanceSelection.instanceSelection(isParamsEx, ISMethodEx, methodOneClass, nameAuxForTables), spark, "IS_"+ nameAuxForTables)

    val reduction = InstanceSelection.infoInstanceSelection(data_sample, dfSampled)

    val metricsIs = Array(timeLSH, timeIS, numeroDeCubetas, maxValue, minValue,
      avgValue, reduction)


    logger.info("reports with IS")

    val factorValues =  lshParamsEx.toArrayStrings() ++
      isParamsEx.toArrayStrings() ++ Array(ISMethodEx) ++
      Array(rate.toString)  ++ Array (executors.toString)

    saveResults(report, factorValues,
      metricsIs,reportPath, spark)

  }




}
