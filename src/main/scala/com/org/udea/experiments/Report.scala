package com.org.udea.experiments

import java.nio.file.{Files, Paths, StandardOpenOption}
import java.nio.charset.StandardCharsets
import java.io._
import org.apache.spark.sql.{Dataset, Row, SaveMode, SparkSession}

class Report(
    var factors: Array[String],
    var metrics: Array[String],
    var sep: String = ","
) {

  var header: String = factors.mkString(sep) + sep + metrics.mkString(sep)

  def setSep(set: String): Unit = {
    sep = set
  }

  def getRowtoAdd(
      factorsValues: Array[String],
      metricsValues: Array[Double]
  ): String = {

    //TODO : include this loger only in debug
    // logger.info("factors: "+ factorsValues.mkString(","))
    //logger.info("metrics: "+ metricsValues.mkString(","))
    val rowToAdd: String = {
      if (
        factorsValues.length == factors.length &&
        metricsValues.length == metrics.length
      ) {
        factorsValues.mkString(sep) + sep + metricsValues.mkString(sep)
      } else {

        throw new IllegalArgumentException(
          "factors values are not consistent: \n" +
            "factor length, factor values length: " +
            factors.length.toString + "," + factorsValues.length.toString + "\n" +
            "factor Header: " + factors.mkString(sep) + "\n" +
            "factorsValues: " + factorsValues.mkString(sep) + "\n" +
            "metrics length, metric values length: " +
            metrics.length.toString + "," + metricsValues.length.toString + "\n" +
            "metrics header: " + metrics.mkString(sep) + "\n" +
            "metricsValues: " + metricsValues.mkString(sep)
        )
      }
    }

    return (rowToAdd)
  }

  def saveReport(
      path: String,
      factorsValues: Array[String],
      metricsValues: Array[Double]
  ): Unit = {

    if (Files.exists(Paths.get(path))) {

      val rowTowrite = "\n" + getRowtoAdd(factorsValues, metricsValues)

      Files.write(
        Paths.get(path),
        rowTowrite.getBytes(StandardCharsets.UTF_8),
        StandardOpenOption.APPEND
      )

    } else {

      val rowTowrite = getRowtoAdd(factorsValues, metricsValues)
      val strToWrite = header + "\n" + rowTowrite
      val f = new File(path)

      f.getParentFile().mkdirs()
      Files.write(Paths.get(path), strToWrite.getBytes(StandardCharsets.UTF_8))
    }
  }

  def getDFandTime(
      instances: Dataset[Row],
      spark: SparkSession,
      name: String
  ): (Dataset[Row], Double) = {

    val t0 = System.nanoTime()
    instances.write
      .mode(SaveMode.Overwrite)
      .format("parquet")
      .save(name + "timeTest/")
    val t1 = System.nanoTime()
    val timeSpent = (t1 - t0) / 6e+10

    val dfSampled = spark.read.parquet(name + "timeTest/")

    (dfSampled, timeSpent)
  }

}
