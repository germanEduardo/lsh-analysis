package com.org.udea.experiments

import com.org.udea.utilities.Constants
import org.apache.spark.mllib.evaluation.BinaryClassificationMetrics
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.DoubleType
import org.apache.spark.sql.{Dataset, SparkSession}


class Metrics[A](dataSet: Dataset[_], labels: Array[A],
                 colPred: String = Constants.COL_PREDICTION) extends Serializable {


  val labelPos = labels(0)
  val labelNeg = labels.filter(_ != labels(0)).toSeq

  val tp = dataSet.where(dataSet(Constants.COL_LABEL) === labelPos and
    dataSet(colPred) === labelPos).count().toDouble

  val fp = dataSet.where(dataSet(Constants.COL_LABEL) <=> labelPos and
    dataSet(colPred) === labelPos).count().toDouble

  val tn = dataSet.where((dataSet(Constants.COL_LABEL) isin (labelNeg: _*)) and
    (dataSet(colPred) isin (labelNeg: _*))).count().toDouble

  val fn = dataSet.where(!(dataSet(Constants.COL_LABEL) isin (labelNeg: _*)) and
    (dataSet(colPred) isin (labelNeg: _*))).count().toDouble

  val sensibility = (tp / (tp + fn)) * 100.0
  val specificity = {(tn / (fp + tn)) * 100.0}
  val precision = {(tp / (tp + fp)) * 100.0}
  val accuracy = ((tp + tn) / (tp + fn + fp + tn)) * 100.0


  def getDesbalancedMetrics(types: String = "main",
                               beta: Double = 0.7, spark: SparkSession): Array[Double] = {
    types match {
      case "ALL" => {
        return (Array(getF1, getGeometricMean, getExcProbability, MCC,
          getWeigthedAccuracy(beta), areaUnderROC(spark)))
      }

      case "main" => {
        return (Array(getF1, getGeometricMean, getWeigthedAccuracy(beta),
          areaUnderROC(spark)))
      }
    }

  }

  def getBasics(): Array[Double] = {
    return (Array(tp, tn, fp, fn, sensibility,specificity,precision,accuracy))
  }

  def getF1(): Double = {((2 * precision * sensibility) / (precision + sensibility)) }
  
  def getGeometricMean(): Double = {math.sqrt(sensibility * specificity)}

  def getExcProbability(): Double = {(tp * tn - fn * fp) / ((fn + tp) * (tn + fp))}

  def MCC(): Double = {(tp * tn - fp * fn) /
    math.sqrt((fn + tp) * (tn + fp) * (fp + tp) * (fn + tn))}


  def getWeigthedAccuracy(beta: Double): Double = {
    (beta * sensibility + (1.0 - beta) * specificity) }


  def areaUnderROC(spark: SparkSession): Double = {

    import spark.implicits._

    val dsRDD : RDD[(Double, Double)] = dataSet.select(
      dataSet(Constants.COL_LABEL).cast(DoubleType).as(Constants.COL_LABEL),
      dataSet(colPred).cast(DoubleType).as(colPred))
      .map(row=> (row.getDouble(0),
        row.getDouble(1))).rdd

    val metInstances = new BinaryClassificationMetrics(dsRDD)
    return (metInstances.areaUnderROC)

  }

}
