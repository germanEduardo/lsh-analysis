package com.org.udea.experiments

import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.utilities.{LogHelper}
import org.apache.spark.ml.Pipeline
import org.apache.spark.sql.{Dataset, Row, SparkSession}

class RFExperiment extends Experiment with LogHelper {

  var runParmsEx: RunParams = _
  var factorsEx: Array[String] = _
  var metricsEx: Array[String] = _


  def set(isParams: IsParams, isMethod: String,
          lshParams: LshParams = null , runParams: RunParams,
          factors: Array[String] = Array() , metrics: Array[String] = Array() ) : Unit = {
    factorsEx = factors
    metricsEx = metrics
    runParmsEx = runParams
  }

  def run(train: Dataset[Row], test: Dataset[Row],
          modelPipe: Pipeline, spark: SparkSession,
          metadataFile: String, nameAuxForTables: String,
          numberPartitions: Int, methodOneClass: String = "one"): Unit = {

    val report = new Report(factorsEx, metricsEx)
    val (trainingData,testingData)  = getTrainTestWithIndex(train, test, metadataFile)
    logger.info("fitting model without IS")
    val modelFittedNoIs =  modelPipe.fit(trainingData)
    logger.info("reports without IS")

    var factorValues = Array("RF")

    saveResults(modelFittedNoIs, trainingData, testingData, report, factorValues,
      Array(), runParmsEx, spark)

  }


}
