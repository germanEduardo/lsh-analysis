package com.org.udea.experiments

import com.org.udea.instanceselection.InstanceSelection
import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.utilities.{LogHelper, Transformations}
import org.apache.spark.ml.Pipeline
import org.apache.spark.sql.{Dataset, Row, SparkSession}

class RandomSamplingExperiment extends Experiment with LogHelper {

  var isParamsEx: IsParams = _
  var runParmsEx: RunParams = _
  var factorsEx: Array[String] = _
  var metricsEx: Array[String] = _
  var ISMethodEx: String = _

  def set(
      isParams: IsParams,
      isMethod: String,
      lshParams: LshParams = null,
      runParams: RunParams,
      factors: Array[String] = Array(),
      metrics: Array[String] = Array()
  ): Unit = {
    factorsEx = factors
    metricsEx = metrics
    runParmsEx = runParams
    ISMethodEx = isMethod
    isParamsEx = isParams
  }

  def run(
      train: Dataset[Row],
      test: Dataset[Row],
      modelPipe: Pipeline,
      spark: SparkSession,
      metadataFile: String,
      nameAuxForTables: String,
      numberPartitions: Int,
      methodOneClass: String = "one"
  ): Unit = {

    val report = new Report(factorsEx, metricsEx)
    logger.info("getting the balanced dataset")
    val (trainingData, testingData) =
      getTrainTestWithIndex(train, test, metadataFile)

    isParamsEx.setInstances(trainingData)
    val (dfSampled, timeIS) = report.getDFandTime(
      InstanceSelection.instanceSelection(isParamsEx, ISMethodEx, ""),
      spark,
      nameAuxForTables + "/IS_"
    )

    val reduction = InstanceSelection.infoInstanceSelection(train, dfSampled)
    val metricsIs = Array(timeIS, reduction)

    val trainingData_IS = Transformations.dfManualIndex(dfSampled, metadataFile)

    logger.info("model fitting with Instance Selection")
    val modelFittedWithIS = modelPipe.fit(trainingData_IS)

    logger.info("reports IS")

    val factorValues = Array(ISMethodEx)

    saveResults(
      modelFittedWithIS,
      trainingData_IS,
      testingData,
      report,
      factorValues,
      metricsIs,
      runParmsEx,
      spark
    )

  }

}
