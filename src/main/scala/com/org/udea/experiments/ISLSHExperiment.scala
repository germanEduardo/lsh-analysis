package com.org.udea.experiments

import com.org.udea.instanceselection.InstanceSelection
import com.org.udea.lsh.LshMethods
import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.utilities.{LogHelper, Transformations}
import org.apache.spark.ml.Pipeline
import org.apache.spark.sql.{Dataset, Row, SparkSession}

class ISLSHExperiment extends Experiment with LogHelper {

  var isParamsEx: IsParams = _
  var lshParamsEx: LshParams = _
  var runParmsEx: RunParams = _
  var factorsEx: Array[String] = _
  var metricsEx: Array[String] = _
  var ISMethodEx: String = _

  def set(
      isParams: IsParams,
      ISmethod: String,
      lshParams: LshParams,
      runParms: RunParams,
      factors: Array[String],
      metrics: Array[String]
  ): Unit = {

    isParamsEx = isParams
    lshParamsEx = lshParams
    factorsEx = factors
    metricsEx = metrics
    ISMethodEx = ISmethod
    runParmsEx = runParms
  }

  def run(
      train: Dataset[Row],
      test: Dataset[Row],
      modelPipe: Pipeline,
      spark: SparkSession,
      metadataFile: String,
      nameAuxForTables: String,
      numberPartitions: Int,
      methodOneClass: String
  ): Unit = {

    val report = new Report(factorsEx, metricsEx)

    val (trainingData, testingData) =
      getTrainTestWithIndex(train, test, metadataFile)

    logger.info("fitting model without IS")
    val modelFittedNoIs = modelPipe.fit(trainingData)

    logger.info("performing Instance Selection")

    val (method, sizeBucket, andFunctions, orFunctions) =
      lshParamsEx.unpackParams()

    val (dfWithSignature, timeLSH) = report.getDFandTime(
      LshMethods.lsh(
        method,
        train,
        spark,
        sizeBucket,
        andFunctions,
        orFunctions,
        numberPartitions
      ),
      spark,
      nameAuxForTables + "/LSH_"
    )

    val (numeroDeCubetas, maxValue, minValue, avgValue) =
      LshMethods.infoLSH(dfWithSignature)

    isParamsEx.setInstances(dfWithSignature)

    val (dfSampled, timeIS) = report.getDFandTime(
      InstanceSelection.instanceSelection(
        isParamsEx,
        ISMethodEx,
        methodOneClass,
        nameAuxForTables
      ),
      spark,
      nameAuxForTables + "/IS_"
    )

    val reduction = InstanceSelection.infoInstanceSelection(train, dfSampled)

    val metricsIs = Array(
      timeLSH,
      timeIS,
      numeroDeCubetas,
      maxValue,
      minValue,
      avgValue,
      reduction
    )

    val trainingData_IS = Transformations.dfManualIndex(dfSampled, metadataFile)

    logger.info("model fitting with Instance Selection")
    val modelFittedWithIS = modelPipe.fit(trainingData_IS)

    logger.info("reports without IS")

    var factorValues = Array("NOIS") ++ lshParamsEx.toArrayStrings() ++
      isParamsEx.toArrayStrings() ++ Array(ISMethodEx)

    saveResults(
      modelFittedNoIs,
      trainingData,
      testingData,
      report,
      factorValues,
      metricsIs,
      runParmsEx,
      spark
    )

    logger.info("reports with IS")

    factorValues = Array("IS") ++ lshParamsEx.toArrayStrings() ++
      isParamsEx.toArrayStrings() ++ Array(ISMethodEx)

    saveResults(
      modelFittedWithIS,
      trainingData_IS,
      testingData,
      report,
      factorValues,
      metricsIs,
      runParmsEx,
      spark
    )

  }

  def runNoIs(
      train: Dataset[Row],
      test: Dataset[Row],
      modelPipe: Pipeline,
      spark: SparkSession,
      metadataFile: String,
      nameAuxForTables: String,
      numberPartitions: Int,
      methodOneClass: String = "one"
  ): Unit = {

    val report = new Report(factorsEx, metricsEx)

    val (trainingData, testingData) =
      getTrainTestWithIndex(train, test, metadataFile)

    logger.info("performing Instance Selection")

    val (method, sizeBucket, andFunctions, orFunctions) =
      lshParamsEx.unpackParams()

    val (dfWithSignature, timeLSH) = report.getDFandTime(
      LshMethods.lsh(
        method,
        train,
        spark,
        sizeBucket,
        andFunctions,
        orFunctions,
        numberPartitions
      ),
      spark,
      nameAuxForTables + "/LSH_"
    )

    val (numeroDeCubetas, maxValue, minValue, avgValue) =
      LshMethods.infoLSH(dfWithSignature)

    isParamsEx.setInstances(dfWithSignature)

    logger.info("sampling")

    val (dfSampled, timeIS) = report.getDFandTime(
      InstanceSelection.instanceSelection(
        isParamsEx,
        ISMethodEx,
        methodOneClass,
        nameAuxForTables
      ),
      spark,
      nameAuxForTables + "/IS_"
    )

    val reduction = InstanceSelection.infoInstanceSelection(train, dfSampled)

    val metricsIs = Array(
      timeLSH,
      timeIS,
      numeroDeCubetas,
      maxValue,
      minValue,
      avgValue,
      reduction
    )

    val trainingData_IS = Transformations.dfManualIndex(dfSampled, metadataFile)

    logger.info("model fitting with Instance Selection")
    val modelFittedWithIS = modelPipe.fit(trainingData_IS)

    logger.info("reports with IS")

    val factorValues = Array("IS") ++ lshParamsEx.toArrayStrings() ++
      isParamsEx.toArrayStrings() ++ Array(ISMethodEx)

    saveResults(
      modelFittedWithIS,
      trainingData_IS,
      testingData,
      report,
      factorValues,
      metricsIs,
      runParmsEx,
      spark
    )

  }

}
