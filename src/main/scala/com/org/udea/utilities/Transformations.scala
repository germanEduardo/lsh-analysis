package com.org.udea.utilities

import org.apache.spark.ml.feature.{StandardScaler, VectorAssembler}
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions.col

import scala.io.Source
import org.apache.spark.sql.types.{Metadata, IntegerType}
import org.apache.spark.sql.functions.{
  lit,
  udf,
  monotonically_increasing_id,
  regexp_replace
}

import scala.util.Try

object Transformations {

  def normalize(df: DataFrame, inputCol: String): DataFrame = {
    val scaler = (new StandardScaler()
      .setInputCol(inputCol)
      .setOutputCol(Constants.COL_SCALED)
      .setWithStd(true)
      .setWithMean(true))
    // Compute summary statistics by fitting the StandardScaler.
    val scalerModel = scaler.fit(df)
    val scaledData: DataFrame = scalerModel.transform(df)
    scaledData

  }

  def hasColumn(df: Dataset[_], path: String): Boolean = Try(df(path)).isSuccess

  def isTableFormatted(dataTable: Dataset[Row]): Boolean = {

    val checkFeatures = hasColumn(dataTable, Constants.COL_FEATURES)
    val checkLabel = hasColumn(dataTable, Constants.COL_LABEL)
    val checkIdn = hasColumn(dataTable, Constants.COL_ID)

    return (checkFeatures && checkLabel && checkIdn)

  }

  def dfManualIndex(
      dataFrameIn: Dataset[Row],
      metaFile: String
  ): Dataset[Row] = {

    val bufferedSource = Source.fromFile(metaFile)
    val file = bufferedSource.getLines.mkString
    bufferedSource.close

    val df = dataFrameIn.withColumn(
      Constants.COL_INDEXED_FEATURES,
      dataFrameIn(Constants.COL_FEATURES)
        .as(Constants.COL_INDEXED_FEATURES, Metadata.fromJson(file))
    )
    return (df)
  }

  def getColsFromFeature(dataframeWithFeatures: Dataset[Row]): Dataset[Row] = {

    val lenFeaVector = dataframeWithFeatures
      .select(Constants.COL_FEATURES)
      .first()
      .getAs[org.apache.spark.ml.linalg.Vector](0)
      .size

    var dfToProcess = dataframeWithFeatures

    for (i <- 0 to (lenFeaVector - 1)) {
      val nameFeature = "Feature" + i

      dfToProcess = dfToProcess.withColumn(
        nameFeature,
        getValue(dfToProcess(Constants.COL_FEATURES), lit(i))
      )

    }

    return (dfToProcess)

  }

  /** check if dataframe and its coluns is unique */
  def isUniqueId(df: Dataset[Row], colId: String): Boolean = {
    val samples = df.count()
    val uniqueId = df.select(colId).distinct().count()
    samples == uniqueId
  }

  def cleanStringCol(df: Dataset[Row], colToClean: String): Dataset[Row] = {
    df.withColumn(
      colToClean,
      regexp_replace(col(colToClean), "\"", "").cast(IntegerType)
    )
  }

  /** Get the format for the spark dataframe
    *
    * fileBase: a file with the columns to process as features
    * tablaBase
    * filters
    * numP
    * colLabel
    * labelsMap
    * colId
    */
  def getDfWithFormat(
      fileBase: Source,
      tablaBase: Dataset[_],
      filters: String,
      numP: Int,
      colLabel: String,
      labelsMap: Map[Int, Int],
      colId: String,
      spark: SparkSession,
      logger: org.apache.log4j.Logger
  ): Dataset[Row] = {
    // reading the file
    val cols = fileBase.getLines.mkString.split(",")
    fileBase.close

    logger.info("using these cols as features: " + cols.mkString(","))

    logger.info("will be used  as label, id: " + colLabel + "," + colId)

    val mapFill: Map[String, Any] = cols.map(x => (x, 0.0)).toMap

    val dataFrameBase =
      if ("".equalsIgnoreCase(filters))
        tablaBase.coalesce(numP).na.fill(mapFill)
      else
        tablaBase.where(filters).coalesce(numP).na.fill(mapFill)

    val assembler = new VectorAssembler()
      .setInputCols(cols)
      .setOutputCol(Constants.COL_FEATURES)

    //set the names col
    val namesCol = assembler.getInputCols

    val finalCols =
      Array(Constants.COL_ID, Constants.COL_FEATURES, Constants.COL_LABEL).map(
        name => col(name)
      )
    var dataFrameBaseWithCols = cleanStringCol(dataFrameBase, colLabel)

    dataFrameBaseWithCols = if (isUniqueId(dataFrameBaseWithCols, colId)) {
      logger.info("the ide is unique")
      dataFrameBaseWithCols
        .withColumn(Constants.COL_LABEL, col(colLabel).cast(IntegerType))
        .withColumn(Constants.COL_ID, col(colId))
    } else {
      logger.info("generating new id")
      logger.info("this process wil fails for large datasets")
      dataFrameBaseWithCols
        .coalesce(1)
        .withColumn(Constants.COL_LABEL, col(colLabel).cast(IntegerType))
        .withColumn(
          Constants.COL_ID,
          monotonically_increasing_id().cast(IntegerType)
        )
      //
    }
    logger.info(
      "checking again the new id (true/false)" + isUniqueId(
        dataFrameBaseWithCols,
        Constants.COL_ID
      )
    )
    dataFrameBaseWithCols =
      dataFrameBaseWithCols.na.drop(Seq(Constants.COL_ID, Constants.COL_LABEL))
    val vectorizedDF = assembler
      .transform(dataFrameBaseWithCols)
      .select(finalCols: _*)
      .withColumn("namesCol", lit(namesCol.mkString(",")))

    val finalDf = vectorizedDF
      .withColumn(
        Constants.COL_LABEL,
        changeLabel(labelsMap)(col(Constants.COL_LABEL))
      )
      .repartition(numP)

    finalDf

  }

  // UDF

  val getValue =
    udf((v: org.apache.spark.ml.linalg.Vector, index: Int) => v.toArray(index))

  def changeLabel(labels: Map[Int, Int]): UserDefinedFunction =
    udf(f = (lb: Int) => {
      val newInd = lb
      val newLabel: Int = labels(newInd)
      newLabel.toInt
    })

  def getOutPutForGCS(path: String, nameAux: String): String = {
    val arraySplit = path.split("/")
    val datasetName = arraySplit.last + "_" + nameAux
    val bucket = arraySplit(2)
    val basePath = arraySplit.slice(3, arraySplit.length - 1).mkString("/")

    "gs://" + bucket + "/" + basePath + "/" + datasetName + "/"
  }

  def getBucketFromPath(path: String): (String, String) = {
    val arraySplit = path.split("/")
    val bucket = arraySplit(2)
    val basePath = arraySplit.slice(3, arraySplit.length - 1).mkString("/")
    (bucket, basePath)
  }

}
