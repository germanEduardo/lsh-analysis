package com.org.udea.utilities

import org.apache.spark.ml.linalg.{DenseVector, Vector, Vectors}
import org.apache.spark.sql.Row
import Numeric.Implicits._

object Mathematics{

  def dot(x: Vector, y: Vector): Double = {
    var k = 0
    var sum = 0.0
    while(k < x.size) {
      sum = sum + (x(k) * y(k))
      k = k + 1
    }
    sum
  }

  def binaryToDec(numBin: Array[Int]): Int = {
    var numDec: Double = 0.0
    var exp: Int = 0
    for(i <- (numBin.length-1) to 0 by -1) {
      numDec += scala.math.pow(2, exp) * numBin(i)
      exp += 1
    }
    numDec.toInt
  }

  def stringSignature(numBin: Array[Int]): String = {
    var stringSignature : String = ""
    for(i <- 0 to (numBin.length-1)) {
      stringSignature = stringSignature + numBin(i).toString
    }
    stringSignature
  }

  def distance(a: Vector, b: Vector): Double = {
    Math.sqrt(Vectors.sqdist(a, b))
  }

  def normalizeVector(a : Vector): Vector ={
    val norm = Vectors.norm(a, 2)
    val values = a.toArray
    Vectors.dense(values.map(_/norm))
  }

  def getAverage(instances:  Array[Array[Double]]): Array[Double] = {
    val len = instances.length
    instances.head.indices.foldLeft(List[Double]()) {
      case (acc, idx) =>
        val sumOfIdx = instances.map(_ (idx)).sum
        acc :+ (sumOfIdx / len)
    }.toArray
  }

  def mean[T: Numeric](xs: Iterable[T]): Double = xs.sum.toDouble / xs.size

  def variance[T: Numeric](xs: Iterable[T]): Double = {
    val avg = mean(xs)

    xs.map(_.toDouble).map(a => math.pow(a - avg, 2)).sum / xs.size
  }

  def stdDev[T: Numeric](xs: Iterable[T]): Double = math.sqrt(variance(xs))

  def meanStdDev[T: Numeric](xs: Iterable[T]): (Double, Double) = {
    val avg = mean(xs)
    val std = stdDev(xs)
    (avg, std)
  }


  def vectorCentroid(instances: Seq[Row], position: Int = 0): Vector = {
   Vectors.dense(getAverage(instances.map(_.getAs[Vector](position).toArray).toArray))
  }



}
