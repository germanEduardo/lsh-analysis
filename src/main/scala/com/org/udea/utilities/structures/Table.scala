package com.org.udea.utilities.structures


case class Id(id:Int, label: Int)

case class Info(distance: Double, id: Int, label: Int)

case class Distances(isUpdate: Boolean, updateIndex: Int, info: Seq[Info])


case class RowTable(id: Id,
                    distances: Distances,
                    neighbors: Seq[Info],
                    enemy: Double,
                    associates: Seq[Int])


final case class NotExistingRow(private val message: String = "",
                                 private val cause: Throwable = None.orNull)
  extends Exception(message, cause)


class Table(){

  var table = Seq[RowTable]()

  def addRow(row: RowTable): Unit = {
    table = table :+ row
  }

  def removeRow(i: Int): Unit = {
    table = table.patch(i, Nil, 1)
  }

  def size (): Int = {
    table.size
  }

  def getRow(i: Int): RowTable = {
    table(i)
  }

  def getTable(): Seq[RowTable] = {
    table
  }

  def getIndexAndRowById(id: Int): (Int, RowTable) = {
    val index = table.indexWhere(_.id.id == id)
    try {
      val row: RowTable = table(index)
      (index, row)
    } catch {
      case _:  java.lang.IndexOutOfBoundsException => throw NotExistingRow("Not existing row"  + id + " in the table" + printTable())
      case c:  Throwable => throw NotExistingRow("uncontrolled error in "  + id + " in the table" + printTable() + "\n" + c.getMessage)
    }
  }

  def replaceRow(index: Int, row: RowTable) {
    table = table.updated(index, row)
  }

  def orderByEnemy() {
    table = table.sortBy(_.enemy).reverse
  }

  def printTable(): String = {
    table.mkString("| \n")
  }

}
