package com.org.udea.utilities

import org.apache.log4j.{Level,Logger}

trait LogHelper{
  val loggerName = this.getClass.getName
  @transient val logger = Logger.getLogger(loggerName)
  logger.setLevel(Level.INFO)
  logger.setLevel(Level.DEBUG)
  Logger.getLogger("org").setLevel(Level.WARN)
  Logger.getLogger("hive").setLevel(Level.WARN)
}
