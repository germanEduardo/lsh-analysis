package com.org.udea.instanceselection

import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Constants
import org.apache.spark.sql.{Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._

object InstanceSelection {

  def minorityClass(df: Dataset[_]): Int = {
    val numOflabels = df
      .groupBy(Constants.COL_LABEL)
      .count
      .orderBy(col("count").asc)
      .select(Constants.COL_LABEL)
      .head
    numOflabels(0).asInstanceOf[Int]
  }

  def get_match_method(
      method: String,
      params: IsParams,
      methodOneClass: String = "one",
      nameAux: String = ""
  ): Dataset[Row] = {

    method match {

      case Constants.INSTANCE_SELECTION_ENTROPY_METHOD => {
        EntropySampling.instanceSelection(params)
      }

      case Constants.INSTANCE_SELECTION_DROP3_METHOD => {
        Drop3Sampling.instanceSelection(params, methodOneClass, nameAux)
      }

      case Constants.INSTANCE_SELECTION_OVER => {
        RandomSampling.OverSampling(params)
      }

      case Constants.INSTANCE_SELECTION_UNDER => {
        RandomSampling.UnderSampling(params)
      }

      case _ =>
        throw new IllegalArgumentException(
          "the Method " + method + "  does not exists"
        )

    }
  }

  /** método para realizar instance selection sobre un conjunto de instancias
    *  @param method: indica el método de instance selection a utilizar, sus posibles valores son:
    *  "entropia", "drop3", "lsh_is_s", "lsh_is_f"
    *  @param instances: conjunto de instancias a las que se les aplicara el instance selection
    *  @param spark: se auto-explica
    *  @param unbalanced: toma los valores de true o false para indicar si el conjunto de instancias
    * es desbalanceado o no respectivamente
    *  @param neighbors: indica el número de vecinos que se usara en el drop3
    *  @param subBuckets: indica el número maximo de muestras por cubeta en el drop3 (1000 por defecto)
    *  @param distancesIntervale: esto hay que quitarlo(poner 100)
    *  @return retorna el dataframe ingresado en el parametro instances luego de aplicarle instance selection
    */
  def instanceSelection(
      method: String,
      instances: Dataset[Row],
      spark: SparkSession,
      unbalanced: Boolean,
      neighbors: Int = 0,
      subBuckets: Int = 1000,
      distancesIntervale: Int = 0
  ): Dataset[_] = {

    val minority = { if (unbalanced) minorityClass(instances) else 0 }

    val params = new IsParams(
      instances,
      unbalanced,
      minority,
      spark,
      neighbors,
      subBuckets,
      distancesIntervale
    )

    get_match_method(method, params)
  }

  def instanceSelection(
      params: IsParams,
      method: String,
      methodOneClass: String
  ): Dataset[Row] = {

    val minority = {
      if (params.getunbalanced) minorityClass(params.getinstances()) else 0
    }

    params.setminorityClass(minority)

    get_match_method(method, params, methodOneClass)

  }

  def instanceSelection(
      params: IsParams,
      method: String,
      methodOneClass: String,
      nameAux: String
  ): Dataset[Row] = {

    val minority = {
      if (params.getunbalanced) minorityClass(params.getinstances()) else 0
    }

    params.setminorityClass(minority)

    get_match_method(method, params, methodOneClass, nameAux)

  }

  def infoInstanceSelection(
      originalSet: Dataset[Row],
      selectedSet: Dataset[Row]
  ): Double = {

    val originalInstances = originalSet.count
    val selectedInstances = selectedSet.count
    selectedInstances.toDouble / originalInstances
  }
}
