package com.org.udea.instanceselection

import org.apache.spark.mllib.tree.impurity.Entropy
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._



class AggEntropyUnbalanced() extends UserDefinedAggregateFunction {

  override def inputSchema: StructType = StructType(Array(StructField("item", IntegerType)))

  override def bufferSchema: StructType =  StructType(Array(
    StructField("labels", MapType(IntegerType, LongType)),
    StructField("total", LongType)
  ))

  override def dataType: DataType = DoubleType

  override def deterministic: Boolean = true

  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    buffer(0) = Map[Int, Long]()
    buffer(1) = 0L
  }

  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    val existLabel = buffer(0).asInstanceOf[Map[Int, Long]].keys.exists(_ == input.getInt(0))

    var newVal: Long = 1
    if (existLabel){
      newVal = buffer(0).asInstanceOf[Map[Int, Long]](input.getInt(0)) + 1
    }
    buffer(0) = buffer(0).asInstanceOf[Map[Int, Long]] + (input.getInt(0) -> newVal)
    buffer(1) = buffer.getLong(1) + 1
  }

  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    buffer1(0) = buffer1(0).asInstanceOf[Map[Int, Long]] ++ buffer2(0).asInstanceOf[Map[Int, Long]]
    buffer1(1) = buffer1.getLong(1) + buffer2.getLong(1)
  }

  override def evaluate(buffer: Row): Any = {
    // compute a entropy that is 1/(number of examples)
    if (buffer(0).asInstanceOf[Map[Int, Long]].size == 1) {
      1.0/buffer.getLong(1)
    } else {
      val numOfInstances = buffer(0).asInstanceOf[Map[Int, Long]].map{case (a,b) => b.toDouble}.toArray
      Entropy.calculate(numOfInstances, buffer.getLong(1))
    }
  }
}


