package com.org.udea.instanceselection

import org.apache.spark.ml.linalg.SQLDataTypes.VectorType
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

class AggKnn() extends UserDefinedAggregateFunction {

  override def inputSchema: StructType = StructType(Array(
    StructField("features", VectorType),
    StructField("idn", IntegerType),
    StructField("label", IntegerType)
  ))

  override def bufferSchema: StructType = StructType(Array(
    StructField("allInfo", ArrayType(StructType(Array(
      StructField("features", VectorType),
      StructField("idn", IntegerType),
      StructField("label", IntegerType)
    ))))
  ))

  override def dataType: DataType = ArrayType(StructType(Array(
    StructField("features", VectorType),
    StructField("idn", IntegerType),
    StructField("label", IntegerType)
  )))

  override def deterministic: Boolean = true

  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    buffer(0) = Array[(Vector, Int, Int)]()
  }

  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    buffer(0) = buffer(0).asInstanceOf[Seq[(Vector, Int, Int)]] :+
      (input(0).asInstanceOf[Vector], input(1).asInstanceOf[Int], input(2).asInstanceOf[Int])
  }

  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
    buffer1(0) = buffer1(0).asInstanceOf[Seq[(Vector, Int, Int)]] ++
      buffer2(0).asInstanceOf[Seq[(Vector, Int, Int)]]
  }

  override def evaluate(buffer: Row): Any = {
    buffer(0)
  }
}
