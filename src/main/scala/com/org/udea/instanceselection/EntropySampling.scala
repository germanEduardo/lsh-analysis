package com.org.udea.instanceselection

import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Constants
import org.apache.spark.sql._

object EntropySampling {

  var totalInstances: Long = _

  // metodo encargado de calcular la entropia por cubeta mediante la UDAF y asignarla a cada muestra
  def addEntropy(
      instances: DataFrame,
      columnNames: (String, String, String),
      aggEntropy: AggEntropyUnbalanced
  ): DataFrame = {
    val (colSignature, colCOL_LABEL, colOutput) = columnNames
    instances
      .groupBy(instances(colSignature))
      .agg(
        aggEntropy(instances.col(colCOL_LABEL))
          .as(colOutput)
      )
  }

  // metodo encargado de seleccionar las instancias en base a una entropia dada mediante
  // la generacion de un numero aleatorio
  def pickInstance(
      entropia: Double,
      COL_LABEL: Int,
      unbalanced: Boolean,
      minorityClass: Int
  ): Boolean = {

    if (COL_LABEL == minorityClass && unbalanced) {
      return true
    }
    val rnd = scala.util.Random.nextFloat
    rnd < entropia
  }

  def instanceSelection(params: IsParams): Dataset[Row] = {
    val (instances, unbalanced, minorityClass, spark, _, _, _) =
      params.unpackParams()
    val aggEntropy = new AggEntropyUnbalanced()

    // se agrega la entropia a cada instacia, esta es calculada en base a las instancias de la cubeta
    val instancesOrd = instances.select(
      Constants.COL_ID,
      Constants.COL_FEATURES,
      Constants.COL_LABEL,
      Constants.COL_SIGNATURE
    )
    val entropyForSignature = addEntropy(
      instancesOrd,
      (Constants.COL_SIGNATURE, Constants.COL_LABEL, Constants.COL_ENTROPY),
      aggEntropy
    )

    spark.sparkContext.broadcast(entropyForSignature)

    //se hace join de de las entropias con los datos originales
    val selectInstances: Dataset[Row] = instancesOrd.join(
      entropyForSignature,
      Seq(Constants.COL_SIGNATURE),
      "left_outer"
    )

    // se hace la seleccion de las instances basada en la entropia obtenida
    // based on a dataframe:
    // Row (signature: String,  idn: Int   features: Vector, label: Int, entropy: Double)
    val selectedInstances = selectInstances
      .filter(row => {
        pickInstance(row.getDouble(4), row.getInt(3), unbalanced, minorityClass)
      })
      .drop(Constants.COL_SIGNATURE, Constants.COL_ENTROPY)
      .dropDuplicates(Constants.COL_ID)

    (selectedInstances)
  }

}
