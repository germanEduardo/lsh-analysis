package com.org.udea.instanceselection

import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Constants
import org.apache.spark.sql.{Dataset, Row}
import org.apache.spark.sql.functions.{array_repeat, col, explode, lit}

object RandomSampling {

  def getRatio[T](
      df: Dataset[T],
      minorityClass: Int
  ): (Int, Dataset[T], Dataset[T]) = {

    val majorDf: Dataset[T] =
      df.filter(col(Constants.COL_LABEL) =!= minorityClass)
    val minorDf: Dataset[T] =
      df.filter(col(Constants.COL_LABEL) === minorityClass)
    val ratio = (majorDf.count() / minorDf.count())

    (ratio.toInt, majorDf, minorDf)

  }

  def OverSampling(params: IsParams): Dataset[Row] = {
    val (instances, _, minorityClass, _, _, _, _) =
      params.unpackParams()

    val (ratio, majorDf, minorDf) = getRatio(instances, minorityClass)
    val overSampled =
      minorDf
        .withColumn("dummy", explode(array_repeat(lit("1"), ratio)))
        .drop("dummy")

    val result = majorDf.union(overSampled)

    result
  }

  def UnderSampling(
      params: IsParams,
      seed: Long = scala.util.Random.nextLong()
  ): Dataset[Row] = {
    val (instances, _, minorityClass, _, _, _, _) =
      params.unpackParams()

    val (ratio, majorDf, minorDf) = getRatio(instances, minorityClass)
    val majorDfSampled = majorDf.sample(false, 1 / ratio.toDouble, seed)
    val result = majorDfSampled.union(minorDf)

    result
  }

}
