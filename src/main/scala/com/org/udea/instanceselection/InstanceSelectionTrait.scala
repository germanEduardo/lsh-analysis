package com.org.udea.instanceselection

import com.org.udea.parameters.IsParams
import org.apache.spark.sql.{DataFrame, Dataset, Row}

trait InstanceSelectionTrait {
  def instanceSelection(params: IsParams): Dataset[_]

}
