package com.org.udea.instanceselection

import com.org.udea.lsh.LshMethods
import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Mathematics.{distance, meanStdDev, vectorCentroid}
import com.org.udea.utilities.Transformations.isUniqueId
import com.org.udea.utilities._
import com.org.udea.utilities.structures._
import org.apache.spark.ml.linalg.{DenseVector, Vector}
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{explode, udf}

/** @author David Augusto Soto A. augusto.soto@udea.edu.co
  *  @version 1.0
  *
  *  Esta clase realiza instance selection mediante el método de DROP3
  */

object Drop3Sampling extends Serializable {

  @transient lazy val log = org.apache.log4j.LogManager.getLogger("for debug")

  final case class Drop3Error(
      private val message: String = "",
      private val cause: Throwable = None.orNull
  ) extends Exception(message, cause)

  def messsageForDrop3Error(instances: Seq[Row], table: Table): String = {

    val instancesNoRow: Seq[(Array[Double], Int, Int)] = instances.map(r =>
      (r.getAs[DenseVector](0).toArray, r.getInt(1), r.getInt(2))
    )

    val instancesNoRowStr: Seq[String] = instancesNoRow.map(ins =>
      ins._1.mkString(",") + '|' +
        ins._2.toString + '|' + ins._3.toString
    )

    "features: \n " + instancesNoRowStr.mkString(
      "\n"
    ) + " \n and the table is \n" + table.printTable()
  }

  def instanceSelection(
      params: IsParams,
      methodOneClass: String = "one",
      nameAux: String = ""
  ): DataFrame = {

    val (
      df,
      unbalanced,
      minorityClass,
      spark,
      k_Neighbors,
      maxBucketSize,
      distancesIntervale
    ) = params.unpackParams()

    require(
      k_Neighbors + 1 < distancesIntervale,
      "El número de vecinos debe ser menor o igual al intervalo de distancias"
    )

    require(
      isUniqueId(df, Constants.COL_ID),
      "the column " + Constants.COL_ID + " should be unique"
    )

    val instances = LshMethods.subBuckets(maxBucketSize, df, spark)
    log.info("using as drop3_subucket: " + nameAux + "/drop3_subucket_")
    instances.write
      .mode(SaveMode.Overwrite)
      .format("parquet")
      .save(nameAux + "/drop3_subucket_")

    log.info(
      "drop 3 in instances partitions: " + instances.rdd.getNumPartitions
    )

    val aggKnn = new AggKnn()

    var instancesWithInfo = instances
      .groupBy(Constants.COL_SIGNATURE)
      .agg(
        aggKnn(
          instances.col(Constants.COL_FEATURES),
          instances.col(Constants.COL_ID),
          instances.col(Constants.COL_LABEL)
        ).as("info")
      )

    val numBuckets = instancesWithInfo
      .select(instancesWithInfo.col(Constants.COL_SIGNATURE))
      .distinct()
      .count()
    instancesWithInfo = instancesWithInfo.coalesce(numBuckets.toInt)

    // repartion based on the # of partitions and the signatures
    log.info(
      "drop 3  in  instanceswith infopartitions: " + instancesWithInfo.rdd.getNumPartitions
    )

    val transformUDF = udf(
      drop3(
        _: Seq[Row],
        _: String,
        distancesIntervale,
        unbalanced,
        minorityClass,
        k_Neighbors,
        methodOneClass
      )
    )

    val remove = instancesWithInfo
      .withColumn(
        "InstancesToEliminate",
        transformUDF(
          instancesWithInfo.col("info"),
          instancesWithInfo.col("signature")
        )
      )
      .drop("info", "signature")

    val explodeDF = remove.select(
      explode(remove("InstancesToEliminate")).as(Constants.COL_ID)
    )

    instances.join(explodeDF, Seq(Constants.COL_ID), "leftanti")

  }

  def eliminateInstances(
      instances: Seq[Row],
      instancesForRemove: Seq[Int]
  ): Seq[Row] = {
    instances.filter(x => !instancesForRemove.contains(x.getInt(1)))
  }

  def isOneClass(instances: Seq[Row], label: Int): Boolean = {
    val label = instances.head.getInt(2)
    !instances.exists(x => x.getInt(2) != label)
  }

  def instancesToRemoveBasedCentroid(instances: Seq[Row]): Seq[Int] = {
    val centroid = vectorCentroid(instances)
    val rowWithDistances = instances.map(row => {
      val id = row.getInt(1)
      val label = row.getInt(2)
      val features = row.getAs[Vector](0)
      val diff = distance(features, centroid)
      Row(id, label, features, diff)
    })

    val distancesVector = rowWithDistances.map(_.getDouble(3))
    val meanStd = meanStdDev(distancesVector)
    val meanDist = meanStd._1
    val stdDist = meanStd._2
    //print(meanDist)
    //print(stdDist)
    // print("centroide", centroid)

    {
      if (stdDist <= 0.001) {
        // the instances are even distributed (no remove any instance)
        List(): Seq[Int]
      } else {
        // remove the instances the difference between centroid is less that the
        // standard deviation
        rowWithDistances
          .filter(row => {
            (row.getDouble(3) <= (meanDist + stdDist)) && (row
              .getDouble(3) >= (meanDist - stdDist))
          })
          .map(_.getInt(0))
      }
    }

  }

  def returnIfOneClass(
      instances: Seq[Row],
      unbalanced: Boolean,
      label: Int,
      minorityClass: Int,
      methodOneClass: String = "one"
  ): Seq[Int] = {

    val isBalancedAndNoMinorityClass =
      !unbalanced || (unbalanced && label != minorityClass)

    if (isBalancedAndNoMinorityClass && methodOneClass == "one") {
      instances.drop(1).map(_.getInt(1)) // one class return any class
    } else if (isBalancedAndNoMinorityClass && methodOneClass == "boundaries") {
      instancesToRemoveBasedCentroid(instances)
    } else {
      List(): Seq[Int]
    }
  }

  // instances is [features, id, label]
  def drop3(
      instances: Seq[Row],
      sign: String,
      distancesIntervale: Int,
      unbalanced: Boolean,
      minorityClass: Int,
      k_Neighbors: Int,
      methodOneClass: String = "one"
  ): Seq[Int] = {

    // check if the instances are k+1 if not return empty list (meaning no instances removed)
    log.info("bucket:" + sign)
    if (instances.size <= k_Neighbors + 1) {
      log.info("bucket wit less that k: " + k_Neighbors + " + 1 neighboorhoods")
      return List(): Seq[Int]
    }

    // get the label
    val label = instances.head.getInt(2)

    if (isOneClass(instances, label)) {
      return returnIfOneClass(
        instances,
        unbalanced,
        label,
        minorityClass,
        methodOneClass
      )
    }

    val datatableInstances = createDataTable(instances)
    var table = completeTable(
      instances,
      distancesIntervale,
      k_Neighbors,
      datatableInstances
    )

    var instancesForRemove = Seq[Int]()
    var numOfInstances = table.size()
    var i = 0
    while (i < numOfInstances) {

      val instance = table.getRow(i)

      val instanceId = instance.id
      val instanceAssociates = instance.associates

      var requireRemove = false

      if ((instanceId.label != minorityClass && unbalanced) || !unbalanced) {
        requireRemove = removeInstance(
          instanceId,
          instanceAssociates,
          table,
          instancesForRemove
        )
      }
      if (requireRemove) {

        table.removeRow(i)
        numOfInstances = numOfInstances - 1
        if (numOfInstances < k_Neighbors) {
          // return the current instances to remove due the table dont enough samples
          return (instancesForRemove)
        }
        table = updateTableForRemove(
          instanceId.id,
          instanceAssociates,
          distancesIntervale,
          instances,
          table,
          instancesForRemove
        )
        instancesForRemove = instancesForRemove :+ instanceId.id
      } else {
        i = i + 1
      }
    }
    instancesForRemove
  }

  def updateTableForRemove(
      instanceToRemove: Int,
      instanceAssociates: Seq[Int],
      delta: Int,
      instances: Seq[Row],
      table: Table,
      instanceRemove: Seq[Int]
  ): Table = {
    var myTable = table
    for (associate <- instanceAssociates) {
      if (!instanceRemove.contains(associate)) {
        try {
          val (index: Int, rowOfAssociate: RowTable) =
            table.getIndexAndRowById(associate)
          val neighborsOfAssociate =
            rowOfAssociate.neighbors // HEre is the error!!!!
          var distances: Distances = rowOfAssociate.distances
          val distancesOfAssociate: Seq[Info] = distances.info
          var isEmptyDistances = distances.isUpdate
          val associatesOfAssociate = rowOfAssociate.associates
          var updateDistances = distancesOfAssociate
          var newNeighbor = None: Option[Info]
          if (!isEmptyDistances || !updateDistances.isEmpty) {
            //preguntar si se debe recalcular distancias y hacerlo en caso de que si
            if (updateDistances.isEmpty) {
              distances =
                recalculateDistances(associate, delta, instances, myTable)
              updateDistances = distances.info
              isEmptyDistances = distances.isUpdate
            }
            //
            if (updateDistances.nonEmpty) {
              newNeighbor = Option(updateDistances.head)
              updateDistances = updateDistances.drop(1)
            }
            // what is the pruporse of this code?

            while (
              instanceRemove.contains(
                newNeighbor.getOrElse(Info(0, 0, 0)).id
              ) &&
              (!isEmptyDistances ||
                updateDistances.nonEmpty)
            ) {

              // here arrive an empty list
              if (updateDistances.nonEmpty) {
                newNeighbor = Option(updateDistances.head)
                updateDistances = updateDistances.drop(1)
              }
              //preguntar si se debe recalcular distancias y hacerlo en caso de que si
              if (updateDistances.isEmpty && !isEmptyDistances) {
                distances =
                  recalculateDistances(associate, delta, instances, myTable)
                updateDistances = distances.info
                isEmptyDistances = distances.isUpdate
              }
            }

          }

          var updateNeighbors =
            neighborsOfAssociate.filter(x => x.id != instanceToRemove)

          newNeighbor match {
            case None =>
            case Some(newNeighborvalue) =>
              if (!instanceRemove.contains(newNeighborvalue.id)) {
                updateNeighbors = updateNeighbors :+ newNeighborvalue
              }
          }

          val newAssociates =
            associatesOfAssociate.filter(x => x != instanceToRemove)
          val dist = Distances(
            distances.isUpdate,
            distances.updateIndex,
            updateDistances
          )
          val row = new RowTable(
            rowOfAssociate.id,
            dist,
            updateNeighbors,
            rowOfAssociate.enemy,
            newAssociates
          )

          myTable.replaceRow(index, row)

          newNeighbor match {
            case None =>
            case Some(newNeighborvalue) =>
              if (!instanceRemove.contains(newNeighborvalue.id)) {

                myTable = updateAssociates(
                  rowOfAssociate.id.id,
                  newNeighborvalue.id,
                  myTable
                )

              }
          }

        } catch {
          case e: Throwable =>
            throw Drop3Error(
              "Drop3 Error!!! \n" +
                messsageForDrop3Error(instances, myTable) +
                "\n And the error \n" + e.getMessage + "\n and the instance to remove is: " + instanceToRemove.toString
            )

        }
      }
    }
    myTable
  }

  def recalculateDistances(
      instanceToUpdate: Int,
      delta: Int,
      instances: Seq[Row],
      table: Table
  ): Distances = {
    val myTable = table
    var myDelta = delta
    var noMore = false
    val (index, rowToUpdate) = myTable.getIndexAndRowById(instanceToUpdate)
    val distanceIndex = rowToUpdate.distances.updateIndex
    val totalInstance = instances.size
    val availableInstance = totalInstance - distanceIndex

    if (availableInstance <= delta) {
      myDelta = availableInstance
      noMore = true
    }

    val instance = instances
      .filter(x => x(1).asInstanceOf[Int] == rowToUpdate.id.id)(0)(0)
      .asInstanceOf[Vector]

    val distances =
      calculateDistances(myDelta, distanceIndex, instance, instances)
    val newDistanceIndex = distanceIndex + myDelta
    val newDistance = Distances(noMore, newDistanceIndex, distances)
    val newRow = RowTable(
      rowToUpdate.id,
      newDistance,
      rowToUpdate.neighbors,
      rowToUpdate.enemy,
      rowToUpdate.associates
    )

    myTable.replaceRow(index, newRow)
    return newDistance
  }

  /** método para calcular las distancias de una instancia contra todas las demas
    *  @param delta: indica cuantas distancias se tomaran
    *  @param distanceIndex: Indica cual fue la ultima distancia tomada
    *  @param sample: Instancia a la cual se le calcularan las distancias
    *  @param instances: Conjunto de instancias con las cuales se calculara la distancia
    *  @return Lista con todas las distancias desde distanceIndex hasta distanceIndex + delta
    */
  def calculateDistances(
      delta: Int,
      distanceIndex: Int,
      sample: Vector,
      instances: Seq[Row]
  ): Seq[Info] = {
    val instanceSize = instances.size
    var distances = Seq[Info]()

    for (i <- 0 to (instanceSize - 1)) {
      val distance =
        Mathematics.distance(sample, instances(i)(0).asInstanceOf[Vector])
      if (distance != 0) {
        distances = distances :+ new Info(
          distance,
          instances(i)(1).asInstanceOf[Int],
          instances(i)(2).asInstanceOf[Int]
        )
      }
    }
    if (!distances.isEmpty) {
      distances = scala.util.Sorting.stableSort(
        distances,
        (i1: Info, i2: Info) => i1.distance < i2.distance
      )
    }
    return distances.slice(distanceIndex, distanceIndex + delta)
  }

  def calculateAllDistances(
      instances: Seq[Row],
      id: Int,
      sample: Vector
  ): Seq[Info] = {
    val instanceSize = instances.size
    var distances = Seq[Info]()

    for (i <- 0 until instanceSize) {
      val distance =
        Mathematics.distance(sample, instances(i)(0).asInstanceOf[Vector])
      if (id != instances(i)(1).asInstanceOf[Int]) {
        distances = distances :+ new Info(
          distance,
          instances(i)(1).asInstanceOf[Int],
          instances(i)(2).asInstanceOf[Int]
        )
      }
    }

    if (!distances.isEmpty) {
      distances = scala.util.Sorting.stableSort(
        distances,
        (i1: Info, i2: Info) => i1.distance < i2.distance
      )
    }
    return distances
  }

  def removeInstance(
      instanceId: Id,
      associates: Seq[Int],
      table: Table,
      instanceRemove: Seq[Int]
  ): Boolean = {
    var withInstanceId: Int = 0
    var withoutInstanceId: Int = 0
    for (associate <- associates) {
      if (!instanceRemove.contains(associate)) {
        val (index, rowOfAssociate) = table.getIndexAndRowById(associate)
        val labelOfAssociate = rowOfAssociate.id.label
        val neighborsOfAssociate = rowOfAssociate.neighbors
        val neighborsOfAssociateWithoutInstance =
          neighborsOfAssociate.filter(x => instanceId.id != x.id)
        val knnWithInstance = knn(neighborsOfAssociate)
        val knnWithoutInstance = knn(neighborsOfAssociateWithoutInstance)
        if (knnWithInstance == labelOfAssociate) {
          withInstanceId = withInstanceId + 1
        }
        if (knnWithoutInstance == labelOfAssociate) {
          withoutInstanceId = withoutInstanceId + 1
        }
      }
    }
    if (withoutInstanceId >= withInstanceId) {
      return true
    }
    return false
  }

  def knn(neighbors: Seq[Info]): Int = {
    // Need to define an anti tied strategy (decrease -1 )
    var labels = Map[Int, Int]()
    for (neighbor <- neighbors) {
      var i = 0
      var newVal = 1
      val existLabel = labels.keys.exists(_ == neighbor.label)
      if (existLabel) {
        newVal = labels(neighbor.label) + 1
      }
      labels = labels + (neighbor.label -> newVal)
    }

    val numOflabels = labels.map { case (a, b) => b.toDouble }.toSeq
    // if the number of samples is the same returns dummy label
    if (!numOflabels.exists(x => x != numOflabels(0))) {
      return -99999
    }

    return labels.maxBy(_._2)._1
  }

  def findNeighbors(
      instances: Seq[Info],
      k_Neighbors: Int,
      needOrder: Boolean
  ): Seq[Info] = {
    if (needOrder) {
      val instancesInOrder = scala.util.Sorting.stableSort(
        instances,
        (i1: Info, i2: Info) => i1.distance < i2.distance
      )
      return instancesInOrder.take(k_Neighbors + 1)
    }
    return instances.take(k_Neighbors + 1)
  }

  def findMyNemesis(
      instances: Seq[Info],
      myLabel: Int,
      needOrder: Boolean
  ): Double = {
    val myEnemies = killFriends(instances, myLabel)

    if (needOrder) {
      val enemiesInOrder = scala.util.Sorting.stableSort(
        myEnemies,
        (i1: Info, i2: Info) => i1.distance < i2.distance
      )
      return enemiesInOrder.head.distance
    }
    return myEnemies.head.distance
  }

  def killFriends(instances: Seq[Info], myLabel: Int): Seq[Info] = {
    instances.filter(x => (x.label != myLabel))
  }

  def completeTable(
      instances: Seq[Row],
      distancesIntervale: Int,
      k_Neighbors: Int,
      table: Table
  ): Table = {
    var myTable = table
    var currentInstance: (Vector, Int, Int) =
      null.asInstanceOf[(Vector, Int, Int)]
    val instanceSize = instances.size
    for (i <- 0 until instanceSize) {
      currentInstance = (
        instances(i)(0).asInstanceOf[Vector],
        instances(i)(1).asInstanceOf[Int],
        instances(i)(2).asInstanceOf[Int]
      )
      val instancesId = Id(currentInstance._2, currentInstance._3)
      var distancesOfCurrentInstance =
        calculateAllDistances(instances, currentInstance._2, currentInstance._1)
      val myEnemy =
        findMyNemesis(distancesOfCurrentInstance, currentInstance._3, false)
      distancesOfCurrentInstance =
        distancesOfCurrentInstance.slice(0, distancesIntervale)
      val myNeighbors =
        findNeighbors(distancesOfCurrentInstance, k_Neighbors, false)

      val (index, row) = myTable.getIndexAndRowById(instancesId.id)
      var isUpdateDistance = false
      if (instances.size <= distancesIntervale) {
        isUpdateDistance = true
      }
      val newRow = RowTable(
        instancesId,
        Distances(
          isUpdateDistance,
          distancesIntervale,
          distancesOfCurrentInstance.drop(k_Neighbors + 1)
        ),
        myNeighbors,
        myEnemy,
        row.associates
      )
      myTable.replaceRow(index, newRow)

      for (neighbor <- myNeighbors) {
        myTable = updateAssociates(instancesId.id, neighbor.id, myTable)
      }
    }
    /*
    , the instances are sorted by distance to their nearest enemy remaining in S, and thus points far from the real decision boundary
    are removed first. This allows points internal to clusters to be removed early in the process,
    even if there were noisy points nearby
     */
    myTable.orderByEnemy
    myTable
  }

  def createDataTable(instances: Seq[Row]): Table = {
    val dist_null = null.asInstanceOf[Distances]
    val neighbors_null = null.asInstanceOf[Seq[Info]]
    val enemy_null = null.asInstanceOf[Double]
    val associates_null = Seq.empty[Int]

    val instanceSize = instances.size
    val table = new Table()
    var row = null.asInstanceOf[RowTable]
    for (i <- 0 until instanceSize) {
      val id_tuple =
        Id(instances(i)(1).asInstanceOf[Int], instances(i)(2).asInstanceOf[Int])
      row = new RowTable(
        id_tuple,
        dist_null,
        neighbors_null,
        enemy_null,
        associates_null
      )
      table.addRow(row)
    }
    table
  }

  def updateAssociates(
      associate: Int,
      instanceForUpdate: Int,
      table: Table
  ): Table = {
    val myTable = table
    val (index, row) = myTable.getIndexAndRowById(instanceForUpdate)
    val newRow = RowTable(
      row.id,
      row.distances,
      row.neighbors,
      row.enemy,
      row.associates :+ associate
    )
    myTable.replaceRow(index, newRow)
    myTable
  }
}
