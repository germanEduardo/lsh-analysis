package com.org.udea.lsh

import com.org.udea.utilities.{Constants}
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.functions.concat
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.col

case class RandomHyperplanesDotProductLSH (dataset_RH: DataFrame,
                                           andsFunctions: Int, orsFunctions: Int,
                                           sizeBucket: Double, spark_RH: SparkSession) extends LSH {


  val rh = new RandomHyperplanesLSH(dataset_RH, andsFunctions, spark_RH)
  val dp = new DotProductLSH(dataset_RH, andsFunctions, orsFunctions, sizeBucket, spark_RH)



  def lsh(colForLsh: String, numberPartitions: Int = 10): DataFrame = {

    val signRh = Constants.COL_SIGNATURE+"_rh"
    val signDp = Constants.COL_SIGNATURE+"_dp"

    val dfWithRhSignature =  rh.lsh(colForLsh, numberPartitions)
      .withColumnRenamed(Constants.COL_SIGNATURE, signRh)

    var dfWithDpSignature =  dp.lsh(colForLsh, numberPartitions)

    dfWithDpSignature = dfWithDpSignature.select(
      dfWithDpSignature(Constants.COL_ID),
      dfWithDpSignature(Constants.COL_SIGNATURE).alias(signDp))

    var dfResult = dfWithRhSignature.join(dfWithDpSignature, Seq(Constants.COL_ID), "inner")

    dfResult = dfResult.withColumn(Constants.COL_SIGNATURE,
      concat(dfResult(signDp), dfResult(signRh)))

    dfResult = dfResult.drop(signDp, signRh)

    return dfResult.repartition(numberPartitions, col(Constants.COL_SIGNATURE))

  }


  def setHyperplanes(set_hyperplanes: Array[Vector]): Unit = {
    rh.setHyperplanes(set_hyperplanes)
  }


  def setFamilies(families: Seq[List[(Vector, Double)]]): Unit = {
    dp.setFamilies(families)
  }

  def hashFunction(instance: Vector,
                   hashFunctions: Array[Vector], familieId: String,
                   FamileFunctions:  List[(Vector, Double)]): String = {

    val signRH: String = rh.hashFunction(instance, hashFunctions)
    val signDP: String = dp.hashFunction(instance, familieId, FamileFunctions)

    return (signDP + signRH)

  }





}
