package com.org.udea.lsh

import com.org.udea.utilities.{Constants, Transformations}
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql._
import org.apache.spark.sql.functions.{udf,col, when, isnull}

object LshMethods{

  def getKeys(hashedDataSet: DataFrame): DataFrame = {
    hashedDataSet.select(Constants.COL_SIGNATURE).distinct()
  }

  def findBucket(bucketsDataSet: DataFrame, key: String): DataFrame = {
    bucketsDataSet.select("*")
      .where(Constants.COL_SIGNATURE + " == '" + key + "'")
  }

  def subBuckets(maxBucketSize: Int, df: DataFrame, spark: SparkSession): DataFrame = {

    import spark.implicits._

    val countBucketDf = df.groupBy(Constants.COL_SIGNATURE).count

    val bigBuckets = countBucketDf.select("*").where(s"count >  $maxBucketSize")
    if (bigBuckets.rdd.isEmpty) {
      return df
    }

    val roulette: (Int, Int) => Double =
      (maxBucketSize: Int, bucketSize: Int ) => 1.toDouble/math.ceil(bucketSize.toDouble/maxBucketSize)

    val rouletteUdf = udf(roulette(maxBucketSize, _ : Int))

    val rouletteDf = bigBuckets.
      withColumn(Constants.COL_ROULETTE, rouletteUdf(bigBuckets("count"))).drop("count")

    var dfWithRouletteValue = df.join(rouletteDf, Seq(Constants.COL_SIGNATURE), "left")

    val getListForRoullete: (String, Double) => Map[String, Double] =
      (sing: String, roulleteVal: Double ) => Map(sing -> roulleteVal)

    val getListForRoulleteUdf = udf(getListForRoullete(_:String,_: Double))

    dfWithRouletteValue = (dfWithRouletteValue
      .withColumn("signAndRoulleteValues",
        getListForRoulleteUdf(dfWithRouletteValue(Constants.COL_SIGNATURE),
          dfWithRouletteValue(Constants.COL_ROULETTE)))
      .drop(Constants.COL_ROULETTE))

    val spinRouletteWithList = udf (spinRoulette(_ : String, _: Map[String, Double]))

    val dfResult = dfWithRouletteValue.withColumn(Constants.COL_SIGNATURE,
      when(isnull(dfWithRouletteValue("signAndRoulleteValues")), dfWithRouletteValue(Constants.COL_SIGNATURE))
        .otherwise(spinRouletteWithList(dfWithRouletteValue(Constants.COL_SIGNATURE),
      dfWithRouletteValue("signAndRoulleteValues")))).drop("signAndRoulleteValues")

    return (dfResult.select(df.columns.map(col(_)):_*))

  }

  def spinRoulette(signature: String, list: Map[String, Double]): String = {

    if (!list.contains(signature)) return signature
    val rnd = scala.util.Random.nextFloat
    val newValueInSignature = Math.ceil(rnd/list(signature)).toInt
    return signature + newValueInSignature.toString
  }



  /** método para realizar LSH sobre un conjunto de instancias
    *  @param method: indica el método de LSH a utilizar, sus poibles valores son:
    *  "hyperplanes", "projection"
    *  @param instances: conjunto de instancias a las que se les aplicara el instance selection
    *  @param spark: se autoexplica
    *  @param sizeBucket: tamaño de w para el metodo de LSH projection
    *  @param andFunctions: numero de ands
    *  @param orFunctions: numero de ors
    *  @return retorna un dataframe con una columna llamada "signature" la cual indica la cubeta de cada instancia
    */
  def lsh(method: String,
          instances: DataFrame,
          spark: SparkSession,
          sizeBucket: Double = 1,
          andFunctions: Int,
          orFunctions: Int = 1,
          numberPartions: Int = 10): DataFrame = {

    val normalizeDF = Transformations.normalize(instances, Constants.COL_FEATURES)

    method match {
      case Constants.LSH_HYPERPLANES_METHOD => {
        val randomHyperplanes = new RandomHyperplanesLSH(normalizeDF, andFunctions, spark)
        return randomHyperplanes.lsh(Constants.COL_SCALED, numberPartions).drop(Constants.COL_SCALED)
      }
      case Constants.LSH_PROJECTION_METHOD => {
        val randomProjection = new DotProductLSH(normalizeDF, andFunctions, orFunctions, sizeBucket, spark)
        return randomProjection.lsh(Constants.COL_SCALED, numberPartions).drop(Constants.COL_SCALED)
      }

      case Constants.LSH_COMBINED_METHOD => {
        val concatenation = new RandomHyperplanesDotProductLSH(normalizeDF, andFunctions, orFunctions, sizeBucket, spark)
        return concatenation.lsh(Constants.COL_SCALED, numberPartions).drop(Constants.COL_SCALED)
      }
      case _ => throw new IllegalArgumentException("El método " + method + " no existe")
    }
  }


  def infoLSH(instances: DataFrame): (Long, Long, Long, Double) = {
    val groupBySiganture = instances.groupBy(Constants.COL_SIGNATURE).count
    val numeroDeCubetas = groupBySiganture.count
    val maxValue = groupBySiganture.groupBy().max("count").collect()(0)(0).asInstanceOf[Long]
    val minValue = groupBySiganture.groupBy().min("count").collect()(0)(0).asInstanceOf[Long]
    val avgValue = groupBySiganture.groupBy().avg("count").collect()(0)(0).asInstanceOf[Double]
    return (numeroDeCubetas, maxValue, minValue, avgValue)
  }





}
