package com.org.udea.lsh

import org.apache.spark.sql.{Dataset, SparkSession}


trait LSH {
  var dataset: Dataset[_] = _
  var numHashTables: Int = _
  var spark: SparkSession = _


  def lsh(colForLsh: String, numberPartitions: Int): Dataset[_]
}

