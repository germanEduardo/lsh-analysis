package com.org.udea.parameters

trait ParamsHelper[A] {

  def unpackParams(): A

}
