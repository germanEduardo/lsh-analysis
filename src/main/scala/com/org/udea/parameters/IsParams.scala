package com.org.udea.parameters

import org.apache.spark.sql.{Dataset, Row, SparkSession}

class IsParams (var instances: Dataset[Row],
                var unbalanced: Boolean,
                var minorityClass: Int,
                var spark: SparkSession,
                var neighbors: Int,
                var subBuckets: Int,
                var distancesIntervale: Int) extends
  ParamsHelper[(Dataset[Row], Boolean, Int, SparkSession, Int, Int, Int)] {


  def this (unbalanced: Boolean, minorityClass: Int,
            spark: SparkSession, neighbors: Int, subBuckets: Int,
            distancesIntervale: Int) {

    this(spark.emptyDataFrame, unbalanced, minorityClass,
      spark, neighbors, subBuckets, distancesIntervale)
  }


  def toArrayStrings (): Array[String] = {
    Array(unbalanced.toString, neighbors.toString,
      subBuckets.toString, distancesIntervale.toString)
  }


  def unpackParams(): (Dataset[Row], Boolean, Int, SparkSession, Int, Int, Int) = {
    (instances, unbalanced, minorityClass, spark, neighbors, subBuckets, distancesIntervale)
  }

  def getunbalanced(): Boolean = {unbalanced}

  def getinstances(): Dataset[Row] = {instances}

  def setminorityClass(set: Int): Unit = {minorityClass=set}

  def setInstances(data: Dataset[Row]): Unit = {instances = data}



}