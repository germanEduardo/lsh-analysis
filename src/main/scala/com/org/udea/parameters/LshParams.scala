package com.org.udea.parameters

class LshParams (method: String,
                 sizeBucket: Double = 1,
                 andFunctions: Int,
                 orFunctions: Int = 1)
  extends ParamsHelper[(String,Double, Int, Int)] {

  override def unpackParams(): (String, Double, Int, Int) = {
    (method, sizeBucket, andFunctions, orFunctions)
  }

  def toArrayStrings(): (Array[String]) = {
    Array(method, sizeBucket.toString, andFunctions.toString, orFunctions.toString)
  }


}