package com.org.udea.parameters

class RunParams(k: Int, labels: Array[Int], modelParms: Array[String], path: String) extends
  ParamsHelper[(Int, Array[Int], Array[String], String)] {

  def unpackParams(): (Int, Array[Int], Array[String], String) = {
    (k, labels, modelParms, path)
  }

  def getK(): Int = {
    return (k)
  }

}
