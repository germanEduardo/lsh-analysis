package com.org.udea.runners

import com.org.udea.SparkSessionWrapper
import com.org.udea.experiments.ScaleExperiment
import com.org.udea.parameters.{IsParams, LshParams}
import com.org.udea.runners.config.ParserScale
import com.org.udea.runners.config.ParserScale.Scale

object RunScale extends Run with SparkSessionWrapper {

  def main(args: Array[String]): Unit = {

    val argsParsed = ParserScale.parseArguments(args)

    argsParsed match {
      case Some(configuration) =>

        val tablaBase = configuration.input
        val numPartitions = configuration.par
        val labels = configuration.labels.toArray
        val outPutFile = "reports/" + configuration.output +".csv"
        val outPathHdfs = "data/" + configuration.output
        val tableOutput = "table_" + configuration.output
        val gridIS = getCasesIS(configuration)
        val methodOneClass = configuration.methodOneclass

        val dataBase = spark.table(tablaBase)

        var numExperiment: Int = 1

          for ((lshParams, isParams, isMethod, rate) <- gridIS) {

             for (_ <- 1 to 5) {
              logger.info("start experiment" +  " " + outPutFile + " " + methodOneClass)

            val experiment = new ScaleExperiment()

              val factors = Array("lshMEthod", "sizeBucket", "ands", "ors",
                "imbalance", "neigh", "subBuckets" , "distance", "isMethod",
                "rate", "executors")
              val metrics = Array( "timeLSH", "timeIS", "buckets", "buckMax", "buckMin", "buckAvg", "reduction")

              val executors = spark.sparkContext.getConf.getInt("spark.executor.instances", 0)
              //spark.conf.get("spark.executor.instances").toInt

              logger.info("fitting only the model with IS")
              experiment.set(isParams, isMethod, lshParams,factors, metrics)
              experiment.run(dataBase,rate, executors, spark,
                configuration.output,numPartitions,methodOneClass, outPutFile)
              numExperiment += 1
              logger.info("finish experiment: " + numExperiment.toString)



            logger.info("finish experiment configuration")
             }

        }


      case None =>
        // arguments are bad, error message will have been displayed
        println(".........arguments are bad...............")


    }

    //spark.stop()

  }

  def getCasesIS (config: Scale):
  Seq[(LshParams, IsParams, String, Double)] = {

    val grid = for {
      andFunctions <- config.andFunctions
      orFunctions <- config.orFunctions
      desbalanced <- config.balanced.map(!_)
      lshMethod  <- config.lshMethod
      isMethod  <- config.instanceSelectionMethod
      sizeBucket  <- config.sizeBucket
      neighbors  <- config.neighbors
      maxBucketSize  <- config.maxBucketSize
      distancesIntervale  <- config.distancesIntervale
      rates <- config.rates

    } yield (new LshParams(lshMethod, sizeBucket, andFunctions, orFunctions),
      new IsParams(desbalanced, 1, spark, neighbors,
        maxBucketSize, distancesIntervale), isMethod, rates)

    (grid)


  }


}
