package com.org.udea.runners

import com.org.udea.SparkSessionWrapper
import com.org.udea.runners.config.ParserPreProcess
import com.org.udea.utilities.Transformations
import org.apache.spark.sql.SaveMode

import scala.io.Source

object PreProcessData extends SparkSessionWrapper with Run {

  def main(args: Array[String]): Unit = {

    val argsParsed = ParserPreProcess.parseArguments(args)

    argsParsed match {

      case Some(configuration) =>
        val tableBase = configuration.input
        val partitions = configuration.par
        val fileBase = configuration.file
        val filter = configuration.filter
        val labels = configuration.labels.map { case (key, value) =>
          key.toInt -> value.toInt
        }
        val outputTable = configuration.output
        val colLabel = configuration.colLabel
        val colId = configuration.colId

        logger.info("reading table: " + tableBase)

        val dataFrameTransformed = Transformations.getDfWithFormat(
          fileBase = Source.fromFile(fileBase),
          tablaBase = spark.table(tableBase),
          filters = filter,
          numP = partitions,
          colLabel = colLabel,
          labelsMap = labels,
          colId = colId,
          spark = spark,
          logger = logger
        )

        logger.info("overwriting table: " + outputTable)

        dataFrameTransformed.write
          .mode(SaveMode.Overwrite)
          .saveAsTable(outputTable)

      case None =>
        // arguments are bad, error message will have been displayed
        println(".........arguments are bad...............")

    }

  }

}
