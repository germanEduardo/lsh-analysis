package com.org.udea.runners

import com.org.udea.SparkSessionWrapper
import com.org.udea.experiments.{RFExperiment}
import com.org.udea.parameters.{RunParams}
import com.org.udea.runners.config.ParserRf
import com.org.udea.runners.config.ParserRf.Rf
import com.org.udea.utilities.Constants
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.RandomForestClassifier

object RunRf extends Run with SparkSessionWrapper {


  def main(args: Array[String]): Unit = {


    val argsParsed = ParserRf.parseArguments(args)

    argsParsed match {
      case Some(configuration) =>

        val tablaBase = configuration.input
        val numPartitions = configuration.par
        val json = configuration.json
        val k = configuration.kfolds
        val isKfoldsComputed = configuration.calculated
        val start = configuration.start
        val labels = configuration.labels.toArray

        val outPutFile = "reports/" + configuration.output +".csv"
        val outPathHdfs = "data/" + configuration.output
        val tableOutput = "table_" + configuration.output

        val tablaFolds = createDeterminedFolds(tablaBase, k, spark, isKfoldsComputed)
        // the k-1 combiantions of for training
        val combFolds = tablaFolds.combinations(k-1).toArray

        val gridModel = getCasesModel(configuration)

        var numExperiment: Int = 1

        for ( kUsed <- start to k) {

          val Array(trainingData, testData) = traingTestSplit(spark,
            tablaFolds(k-kUsed),combFolds(kUsed-1), tableOutput, numPartitions)

          for ((trees, depth) <- gridModel) {

            logger.info("start experiment " + kUsed.toString)

              val modelArray = Array(trees.toString, depth.toString)
              val runParams = new RunParams(kUsed, labels, modelArray, outPutFile)

              val experiment = new RFExperiment()

              val factors = Array("rf_type", "trees", "depth", "type", "k")
              val metrics = Array("tp", "tn", "fp", "fn", "sensibility", "specificity", "precision",
                "accuracy", "F1", "Gmean" , "wtdAcc", "AreaROC")

              experiment.set(null, null, null, runParams,factors, metrics)

              val rf = new RandomForestClassifier()
                .setLabelCol(Constants.COL_LABEL)
                .setFeaturesCol(Constants.COL_INDEXED_FEATURES)
                .setNumTrees(trees)
                .setMaxDepth(depth)
                .setMaxMemoryInMB(512)

              val model = new Pipeline().setStages(Array(rf))
              logger.info("fitting with model  wihtout IS")
              experiment.run(trainingData, testData, model, spark, json, configuration.output, numPartitions)

              numExperiment += 1
              logger.info("finish experiment: " + numExperiment.toString)
            }
          logger.info("finish experiment configuration")

        }


      case None =>
        // arguments are bad, error message will have been displayed
        println(".........arguments are bad...............")


    }

    //spark.stop()

  }
   // TODO : check how can generalize this into Run
  def getCasesModel(config: Rf): Seq[(Int, Int)] = {

    val grid = for {
      t <- config.trees
      d <- config.depth

    } yield(t,d)

     grid

  }



}
