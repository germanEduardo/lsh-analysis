package com.org.udea.runners

import com.org.udea.SparkSessionWrapper
import com.org.udea.experiments.RandomSamplingExperiment
import com.org.udea.parameters.{IsParams, RunParams}
import com.org.udea.runners.config.ParserRandom
import com.org.udea.runners.config.ParserRandom.RandomParams
import com.org.udea.utilities.Constants
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.RandomForestClassifier

import scala.collection.JavaConverters._

object RunRandom extends Run with SparkSessionWrapper {

  def main(args: Array[String]): Unit = {

    val argsParsed = ParserRandom.parseArguments(args)

    argsParsed match {
      case Some(configuration) =>
        val tablaBase = configuration.input
        val numPartitions = configuration.par
        val json = configuration.json
        val k = configuration.kfolds
        val isKfoldsComputed = configuration.calculated
        val start = configuration.start
        val labels = configuration.labels.toArray

        val isGCPPath = configuration.input contains "gs"
        val (outPutFile, pathToUpload, tableOutput) =
          getPathNames(isGCPPath, configuration.output)

        val tablaFolds =
          createDeterminedFolds(
            tablaBase,
            k,
            spark,
            isKfoldsComputed,
            isGCPPath = isGCPPath
          )

        logger.info("folds are generated")

        // the k-1 combinations of for training.
        val combFolds = tablaFolds.combinations(k - 1).toArray

        val gridModel = getCasesModel(configuration)
        val gridIS = getCasesIS(configuration)

        var numExperiment: Int = 1

        for (kUsed <- start to k) {

          val Array(trainingData, testData) = traingTestSplit(
            spark,
            tablaFolds(k - kUsed),
            combFolds(kUsed - 1),
            tableOutput,
            numPartitions,
            isGCPPath = isGCPPath
          )

          for ((isParams, isMethod) <- gridIS) {

            for ((trees, depth) <- gridModel) {

              logger.info(
                "start experiment. k:" + kUsed.toString + " out file:" + outPutFile
              )
              logger.info(
                isMethod + " " + isParams.toArrayStrings.mkString(",")
              )
              val modelArray = Array(trees.toString, depth.toString)

              val runParams =
                new RunParams(kUsed, labels, modelArray, outPutFile)

              val experiment = new RandomSamplingExperiment()

              val factors = Array(
                "isMethod",
                "trees",
                "depth",
                "type",
                "k"
              )
              val metrics = Array(
                "tp",
                "tn",
                "fp",
                "fn",
                "sensibility",
                "specificity",
                "precision",
                "accuracy",
                "F1",
                "Gmean",
                "wtdAcc",
                "AreaROC",
                "timeIS",
                "reduction"
              )

              experiment.set(
                isParams,
                isMethod,
                null,
                runParams,
                factors,
                metrics
              )

              val rf = new RandomForestClassifier()
                .setLabelCol(Constants.COL_LABEL)
                .setFeaturesCol(Constants.COL_INDEXED_FEATURES)
                .setNumTrees(trees)
                .setMaxDepth(depth)
                .setMaxMemoryInMB(512)

              val model = new Pipeline().setStages(Array(rf))

              logger.info("fitting only the model with IS")
              experiment.run(
                trainingData,
                testData,
                model,
                spark,
                json,
                configuration.output,
                numPartitions
              )
              logger.info("finish experiment: " + numExperiment.toString)
              UploadToGcs(isGCPPath, outPutFile, pathToUpload, numExperiment)
              numExperiment += 1

            }

          }

          logger.info("finish experiment configuration")

        }

      case None =>
        // arguments are bad, error message will have been displayed
        println(".........arguments are bad...............")

    }

  }

  def getCasesModel(config: RandomParams): Seq[(Int, Int)] = {

    val grid = for {
      t <- config.trees
      d <- config.depth

    } yield (t, d)

    (grid)

  }

  def getCasesIS(config: RandomParams): Seq[(IsParams, String)] = {

    val grid = for {
      isMethod <- config.instanceSelectionMethod

    } yield (
      new IsParams(
        true,
        1,
        spark,
        0,
        0,
        0
      ),
      isMethod
    )

    grid

  }

}
