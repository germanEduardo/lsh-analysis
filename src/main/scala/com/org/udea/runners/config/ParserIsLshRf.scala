package com.org.udea.runners.config

object ParserIsLshRf {

  case class IsLshRf(
      input: String = "base_test",
      par: Int = 250,
      json: String = "categoryMapsNoMon.json",
      labels: Seq[Int] = Array(1, 0),
      output: String = "output",
      calculated: Boolean = false,
      kfolds: Int = 5,
      start: Int = 1,
      trees: Seq[Int] = Seq(50),
      depth: Seq[Int] = Seq(10),
      andFunctions: Seq[Int] = Seq(2),
      orFunctions: Seq[Int] = Seq(0),
      balanced: Seq[Boolean] = Seq(false, true),
      lshMethod: Seq[String] = Seq("projection", "hyperplanes"),
      instanceSelectionMethod: Seq[String] = Seq("entropy", "drop3"),
      sizeBucket: Seq[Double] = Seq(0.1),
      neighbors: Seq[Int] = Seq(2),
      maxBucketSize: Seq[Int] = Seq(2),
      distancesIntervale: Seq[Int] = Seq(4),
      noIs: Boolean = true,
      methodOneclass: String = "one"
  )

  val parser = new scopt.OptionParser[IsLshRf]("Run LSH and IS analisis") {
    head("Run LSH and IS analisis", "0.1")

    opt[String]('I', "input")
      .action((x, c) => c.copy(input = x))
      .text("imput table")

    opt[Int]('p', "par")
      .action((x, c) => c.copy(par = x))
      .text("par is an integer of num of partions")

    opt[String]('J', "json")
      .action((x, c) => c.copy(json = x))
      .text("json of the tabla of input")

    opt[Seq[Int]]('L', "labels")
      .valueName("oldLabel1=newLabel1,oldLabel2=newLabel2...")
      .action((x, c) => c.copy(labels = x))
      .text("labels")

    opt[String]('o', "output")
      .action((x, c) => c.copy(output = x))
      .text("nameof the outfiles")

    opt[Boolean]('C', "calculated")
      .valueName("flag of the folds calculated?")
      .action((x, c) => c.copy(calculated = x))
      .text("are the folds calculated?")

    opt[Int]('k', "kfolds")
      .action((x, c) => c.copy(kfolds = x))
      .text("kfolds is an integer of num of folds")

    opt[Int]('S', "start")
      .action((x, c) => c.copy(start = x))
      .text("start folds i")

    opt[Seq[Int]]('t', "trees")
      .valueName("<trees1>,<trees1>...")
      .action((x, c) => c.copy(trees = x))
      .text("trees to evaluate")

    opt[Seq[Int]]('d', "depth")
      .valueName("<depth1>,<depth2>...")
      .action((x, c) => c.copy(depth = x))
      .text("depth to evaluate")

    opt[Seq[Int]]('A', "andFunctions")
      .action((x, c) => c.copy(andFunctions = x))
      .text("number of and functions")

    opt[Seq[Int]]('O', "orFunctions")
      .action((x, c) => c.copy(orFunctions = x))
      .text("number of or functions")

    opt[Seq[Boolean]]('B', "balanced")
      .valueName("flag is IS balanced?")
      .action((x, c) => c.copy(balanced = x))
      .text("are the IS balanced?")

    opt[Seq[String]]('l', "lshMethod")
      .action((x, c) => c.copy(lshMethod = x))
      .text("name of lsh method")

    opt[Seq[String]]('z', "instanceSelectionMethod")
      .action((x, c) => c.copy(instanceSelectionMethod = x))
      .text("name of instanceSelection method")

    opt[Seq[Double]]('s', "sizeBucket")
      .action((x, c) => c.copy(sizeBucket = x))
      .text("bucket size for randomProjection")

    opt[Seq[Int]]('n', "neighbors")
      .action((x, c) => c.copy(neighbors = x))
      .text("number of neighbors")

    opt[Seq[Int]]('m', "maxBucketSize")
      .action((x, c) => c.copy(maxBucketSize = x))
      .text("max number of isntances per bucket")

    opt[Seq[Int]]('D', "distancesIntervale")
      .action((x, c) => c.copy(distancesIntervale = x))
      .text("number of distances calculates in DROP3")

    opt[Boolean]('N', "noIs")
      .valueName("flag is the model without IS is computed?")
      .action((x, c) => c.copy(noIs = x))
      .text("are the IS balanced?")

    opt[String]('c', "methodOneclass")
      .action((x, c) => c.copy(methodOneclass = x))
      .text("what method in drop3 for one class buckets : one or boundaries")

    help("help").text("prints this usage text")

  }

  def parseArguments(args: Array[String]): Option[IsLshRf] = {
    return parser.parse(args, IsLshRf())

  }

}
