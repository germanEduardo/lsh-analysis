package com.org.udea.runners.config

object ParserRandom {

  case class RandomParams(
      input: String = "base_test",
      par: Int = 250,
      json: String = "categoryMapsNoMon.json",
      labels: Seq[Int] = Array(1, 0),
      output: String = "output",
      calculated: Boolean = false,
      kfolds: Int = 5,
      start: Int = 1,
      trees: Seq[Int] = Seq(50),
      depth: Seq[Int] = Seq(10),
      instanceSelectionMethod: Seq[String] = Seq("undersampling")
  )

  val parser = new scopt.OptionParser[RandomParams]("Run LSH and IS analisis") {
    head("Run LSH and IS analisis", "0.1")

    opt[String]('I', "input")
      .action((x, c) => c.copy(input = x))
      .text("imput table")

    opt[Int]('p', "par")
      .action((x, c) => c.copy(par = x))
      .text("par is an integer of num of partions")

    opt[String]('J', "json")
      .action((x, c) => c.copy(json = x))
      .text("json of the tabla of input")

    opt[Seq[Int]]('L', "labels")
      .valueName("oldLabel1=newLabel1,oldLabel2=newLabel2...")
      .action((x, c) => c.copy(labels = x))
      .text("labels")

    opt[String]('o', "output")
      .action((x, c) => c.copy(output = x))
      .text("nameof the outfiles")

    opt[Boolean]('C', "calculated")
      .valueName("flag of the folds calculated?")
      .action((x, c) => c.copy(calculated = x))
      .text("are the folds calculated?")

    opt[Int]('k', "kfolds")
      .action((x, c) => c.copy(kfolds = x))
      .text("kfolds is an integer of num of folds")

    opt[Int]('S', "start")
      .action((x, c) => c.copy(start = x))
      .text("start folds i")

    opt[Seq[Int]]('t', "trees")
      .valueName("<trees1>,<trees1>...")
      .action((x, c) => c.copy(trees = x))
      .text("trees to evaluate")

    opt[Seq[Int]]('d', "depth")
      .valueName("<depth1>,<depth2>...")
      .action((x, c) => c.copy(depth = x))
      .text("depth to evaluate")

    opt[Seq[String]]('z', "instanceSelectionMethod")
      .action((x, c) => c.copy(instanceSelectionMethod = x))
      .text("name of instanceSelection method")

    help("help").text("prints this usage text")

  }

  def parseArguments(args: Array[String]): Option[RandomParams] = {
    return parser.parse(args, RandomParams())

  }

}
