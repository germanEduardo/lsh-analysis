package com.org.udea.runners.config

object ParserScale {

  case class Scale(input: String= "base_test", par: Int = 250,
                   labels: Seq[Int] = Array(1,0),
                   output: String = "output",
                   andFunctions: Seq[Int] = Seq(2),
                   orFunctions: Seq[Int] = Seq(0),
                   balanced: Seq[Boolean]=Seq(false,true),
                   lshMethod: Seq[String] = Seq("projection","hyperplanes"),
                   instanceSelectionMethod: Seq[String] = Seq("entropy","drop3"),
                   sizeBucket: Seq[Double] = Seq(0.1),
                   neighbors: Seq[Int] = Seq(2),
                   maxBucketSize: Seq[Int] = Seq(2),
                   distancesIntervale: Seq[Int] = Seq(4),
                   methodOneclass : String = "one",
                   rates: Seq[Double] = Seq(0.1,0.2,0.5, 0.8, 0.9, 1.0))

  val parser = new scopt.OptionParser[Scale]("Run LSH and IS analisis") {
    head("Run LSH and IS scale analisis", "0.1")

    opt[String]('I', "input").action( (x, c) =>
      c.copy(input = x) ).text("imput table")

    opt[Int]('p', "par").action( (x, c) =>
      c.copy(par = x) ).text("par is an integer of num of partions")

    opt[Seq[Int]]('L', "labels").valueName("oldLabel1=newLabel1,oldLabel2=newLabel2...").action( (x, c) =>
      c.copy(labels = x) ).text("labels")

    opt[String]('o', "output").action( (x, c) =>
      c.copy(output = x) ).text("nameof the outfiles")

    opt[Seq[Int]]('A', "andFunctions").action((x, c) =>
      c.copy(andFunctions = x)).text("number of and functions")

    opt[Seq[Int]]('O', "orFunctions").action((x, c) =>
      c.copy(orFunctions = x)).text("number of or functions")

    opt[Seq[Boolean]]('B', "balanced").valueName("flag is IS balanced?").action( (x,c) =>
      c.copy(balanced = x) ).text("are the IS balanced?")

    opt[Seq[String]]('l', "lshMethod").action ((x, c) =>
      c.copy(lshMethod = x)).text("name of lsh method")

    opt[Seq[String]]('z', "instanceSelectionMethod").action ((x, c) =>
      c.copy(instanceSelectionMethod = x)).text("name of instanceSelection method")

    opt[Seq[Double]]('s', "sizeBucket").action((x, c) =>
      c.copy(sizeBucket = x)).text("bucket size for randomProjection")

    opt[Seq[Int]]('n', "neighbors").action ((x, c) =>
      c.copy(neighbors = x)).text("number of neighbors")

    opt[Seq[Int]]('m', "maxBucketSize").action( (x, c) =>
      c.copy(maxBucketSize = x) ).text("max number of isntances per bucket")

    opt[Seq[Int]]('D', "distancesIntervale").action ( (x, c) =>
      c.copy(distancesIntervale = x) ).text("number of distances calculates in DROP3")

    opt[String]('c', "methodOneclass").action( (x, c) =>
      c.copy(methodOneclass = x) ).text("what method in drop3 for one class buckets : one or boundaries")

    opt[Seq[Double]]('r', "rates").action((x, c) =>
      c.copy(rates = x)).text("rates to sample the dataframe")


    help("help").text("prints this usage text")
  }

  def parseArguments (args: Array[String]) : Option[Scale] = {
    return parser.parse(args, Scale())

  }

}
