package com.org.udea.runners.config

object ParserPreProcess {

  case class PreProcesConfig(
      input: String = "base_test",
      par: Int = 250,
      file: String = "categoryMapsNoMon.json",
      filter: String = "",
      labels: Map[String, String] = Map("-1" -> "0", "1" -> "1"),
      output: String = "output",
      colLabel: String = "label",
      colId: String = "idn"
  )

  val parser = new scopt.OptionParser[PreProcesConfig]("Preprocess Dataset") {
    head("Preprocess Dataset", "0.1")

    opt[String]('I', "input")
      .action((x, c) => c.copy(input = x))
      .text("input table")

    opt[Int]('p', "par")
      .action((x, c) => c.copy(par = x))
      .text("par is an integer of num of partions")

    opt[String]('F', "file")
      .action((x, c) => c.copy(file = x))
      .text("file with information of columns to features")

    opt[String]('f', "filter")
      .action((x, c) => c.copy(filter = x))
      .text("filter that will be used in where clause")

    opt[Map[String, String]]('L', "labels")
      .valueName("oldLabel1=newLabel1,oldLabel2=newLabel2...")
      .action((x, c) => c.copy(labels = x))
      .text("labels")

    opt[String]('o', "output")
      .action((x, c) => c.copy(output = x))
      .text("nameof the outfiles")

    opt[String]('l', "colLabel")
      .action((x, c) => c.copy(colLabel = x))
      .text("nameof col Label")

    opt[String]('i', "colId")
      .action((x, c) => c.copy(colId = x))
      .text("nameof col Label")

    help("help").text("prints this usage text")

  }

  def parseArguments(args: Array[String]): Option[PreProcesConfig] = {
    return parser.parse(args, PreProcesConfig())

  }

}
