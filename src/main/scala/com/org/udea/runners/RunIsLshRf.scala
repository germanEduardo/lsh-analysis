package com.org.udea.runners

import com.org.udea.SparkSessionWrapper
import com.org.udea.experiments.ISLSHExperiment
import com.org.udea.parameters.{IsParams, LshParams, RunParams}
import com.org.udea.runners.config.ParserIsLshRf
import com.org.udea.runners.config.ParserIsLshRf.IsLshRf
import com.org.udea.utilities.Constants
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.RandomForestClassifier

object RunIsLshRf extends Run with SparkSessionWrapper {

  def main(args: Array[String]): Unit = {

    val argsParsed = ParserIsLshRf.parseArguments(args)

    argsParsed match {
      case Some(configuration) =>
        val isModelNoIs = configuration.noIs
        val tablaBase = configuration.input

        val numPartitions = configuration.par
        val json = configuration.json
        val k = configuration.kfolds
        val isKfoldsComputed = configuration.calculated
        val start = configuration.start
        val labels = configuration.labels.toArray

        val isGCPPath = configuration.input contains "gs"
        val (outPutFile, pathToUpload, tableOutput) =
          getPathNames(isGCPPath, configuration.output)

        val tablaFolds =
          createDeterminedFolds(
            tablaBase,
            k,
            spark,
            isKfoldsComputed,
            isGCPPath = isGCPPath
          )

        logger.info("folds are generated")
        // the k-1 combiantions of for training
        val combFolds = tablaFolds.combinations(k - 1).toArray

        val gridModel = getCasesModel(configuration)
        val gridIS = getCasesIS(configuration)
        val methodOneClass = configuration.methodOneclass

        var numExperiment: Int = 1

        for (kUsed <- start to k) {

          val Array(trainingData, testData) = traingTestSplit(
            spark,
            tablaFolds(k - kUsed),
            combFolds(kUsed - 1),
            tableOutput,
            numPartitions,
            isGCPPath
          )

          for ((lshParams, isParams, isMethod) <- gridIS) {

            for ((trees, depth) <- gridModel) {

              logger.info(
                "start experiment. k:" + kUsed.toString + " out file:" + outPutFile + " one class:" + methodOneClass
              )
              logger.info(
                "with: " + lshParams.toArrayStrings.mkString(
                  ","
                ) + " " + isMethod + " " + isParams.toArrayStrings.mkString(",")
              )
              val modelArray = Array(trees.toString, depth.toString)
              val runParams =
                new RunParams(kUsed, labels, modelArray, outPutFile)

              val experiment = new ISLSHExperiment()

              val factors = Array(
                "instanceSelection",
                "lshMEthod",
                "sizeBucket",
                "ands",
                "ors",
                "imbalance",
                "neigh",
                "subBuckets",
                "distance",
                "isMethod",
                "trees",
                "depth",
                "type",
                "k"
              )
              val metrics = Array(
                "tp",
                "tn",
                "fp",
                "fn",
                "sensibility",
                "specificity",
                "precision",
                "accuracy",
                "F1",
                "Gmean",
                "wtdAcc",
                "AreaROC",
                "timeLSH",
                "timeIS",
                "buckets",
                "buckMax",
                "buckMin",
                "buckAvg",
                "reduction"
              )

              experiment.set(
                isParams,
                isMethod,
                lshParams,
                runParams,
                factors,
                metrics
              )

              val rf = new RandomForestClassifier()
                .setLabelCol(Constants.COL_LABEL)
                .setFeaturesCol(Constants.COL_INDEXED_FEATURES)
                .setNumTrees(trees)
                .setMaxDepth(depth)
                .setMaxMemoryInMB(512)

              val model = new Pipeline().setStages(Array(rf))

              if (isModelNoIs) {

                logger.info("fitting with model wihtout IS")
                experiment.run(
                  trainingData,
                  testData,
                  model,
                  spark,
                  json,
                  configuration.output,
                  numPartitions,
                  methodOneClass
                )

              } else {
                logger.info("fitting only the model with IS")
                experiment.runNoIs(
                  trainingData,
                  testData,
                  model,
                  spark,
                  json,
                  configuration.output,
                  numPartitions,
                  methodOneClass
                )

              }

              numExperiment += 1
              UploadToGcs(isGCPPath, outPutFile, pathToUpload, numExperiment)
            }

          }

          logger.info("finish experiment configuration")

        }

      case None =>
        // arguments are bad, error message will have been displayed
        println(".........arguments are bad...............")

    }

  }

  def getCasesModel(config: IsLshRf): Seq[(Int, Int)] = {

    val grid = for {
      t <- config.trees
      d <- config.depth

    } yield (t, d)

    return (grid)

  }

  def getCasesIS(config: IsLshRf): Seq[(LshParams, IsParams, String)] = {

    val grid = for {
      andFunctions <- config.andFunctions
      orFunctions <- config.orFunctions
      desbalanced <- config.balanced.map(!_)
      lshMethod <- config.lshMethod
      isMethod <- config.instanceSelectionMethod
      sizeBucket <- config.sizeBucket
      neighbors <- config.neighbors
      maxBucketSize <- config.maxBucketSize
      distancesIntervale <- config.distancesIntervale

    } yield (
      new LshParams(lshMethod, sizeBucket, andFunctions, orFunctions),
      new IsParams(
        desbalanced,
        1,
        spark,
        neighbors,
        maxBucketSize,
        distancesIntervale
      ),
      isMethod
    )

    return (grid)

  }

}
