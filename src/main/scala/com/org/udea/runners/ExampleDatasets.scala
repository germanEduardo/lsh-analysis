package com.org.udea.runners
import com.org.udea.SparkSessionWrapper
import com.org.udea.instanceselection.InstanceSelection
import com.org.udea.lsh.LshMethods
import com.org.udea.parameters.IsParams
import com.org.udea.utilities.Constants
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions.{col, isnull, lit, when}

object ExampleDatasets extends SparkSessionWrapper with Run {

  def main(args: Array[String]): Unit = {

    val tableNames =
      Seq("dummy_data_v1", "dummy_data_v2", "dummy_data_v3", "dummy_data_v4")
    val lshs = Seq(
      Constants.LSH_HYPERPLANES_METHOD,
      Constants.LSH_PROJECTION_METHOD,
      Constants.LSH_COMBINED_METHOD
    )
    val ands = Seq(2, 4, 6, 8, 10)
    val isMethods = Seq(
      Constants.INSTANCE_SELECTION_ENTROPY_METHOD,
      Constants.INSTANCE_SELECTION_DROP3_METHOD
    )
    val oneClasses = Seq("one", "boundaries")
    val isParamsEx = new IsParams(true, 0, spark, 4, 1000, 6)

    val gridConf = getGridConf(tableNames, lshs, ands, isMethods, oneClasses)

    for ((name, lshMet, andFunctions, isMet, oneClassMet) <- gridConf) {

      if (
        isMet == Constants.INSTANCE_SELECTION_ENTROPY_METHOD & oneClassMet == "boundaries"
      ) {

        val pathToSave = name + "_" + lshMet + "_" + andFunctions
          .toString() + "_" + isMet + "_" + oneClassMet
        logger.info("ignoring for: " + pathToSave)

      } else {

        val pathToSave = name + "_" + lshMet + "_" + andFunctions
          .toString() + "_" + isMet + "_" + oneClassMet
        logger.info("starting for: " + pathToSave)
        val train = spark.table(name)
        val trainSign =
          LshMethods.lsh(lshMet, train, spark, 0.1, andFunctions, 1, 10)
        isParamsEx.setInstances(trainSign)
        val reducedData = InstanceSelection.instanceSelection(
          isParamsEx,
          isMet,
          oneClassMet,
          name + "_aux"
        )
        val reducedDataFlag = reducedData.withColumn("isSelected", lit(1))
        val originalVsSampled = train.join(
          reducedDataFlag.select("idn", "isSelected"),
          Seq("idn"),
          "left"
        )
        val originalVsSampledSave = originalVsSampled
          .withColumn(
            "isSelected",
            when(isnull(col("isSelected")), 0).otherwise(1)
          )
        originalVsSampledSave
          .select("idn", "feature_1", "feature_2", "label", "isSelected")
          .repartition(1)
          .write
          .mode(SaveMode.Overwrite)
          .format("csv")
          .save("datasetExample/" + pathToSave)
      }

    }

  }

  def getGridConf(
      tables: Seq[String],
      lshsMets: Seq[String],
      andsF: Seq[Int],
      instanceSel: Seq[String],
      oneClass: Seq[String]
  ): Seq[(String, String, Int, String, String)] = {

    val grid = for {
      t <- tables
      lshMe <- lshsMets
      a <- andsF
      is <- instanceSel
      ones <- oneClass

    } yield (t, lshMe, a, is, ones)
    grid
  }

}
