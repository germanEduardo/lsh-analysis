package com.org.udea.runners

import com.google.cloud.storage.{BlobId, BlobInfo, StorageOptions}
import com.org.udea.utilities.Transformations.getBucketFromPath
import com.org.udea.utilities.{LogHelper, Transformations}
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import java.nio.file.{Files, Paths}
import scala.collection.JavaConverters._

trait Run extends LogHelper {

  def createDeterminedFolds(
      table: String,
      kfolds: Int,
      spark: SparkSession,
      isComputed: Boolean,
      seed: Long = 1L,
      isGCPPath: Boolean = false
  ): Array[String] = {

    val dfTable =
      if (isGCPPath) spark.read.parquet(table) else spark.table(table)

    val isFormatted = Transformations.isTableFormatted(dfTable)
    var names = Array.range(1, kfolds + 1).map(table + "_" + _.toString)

    if (isGCPPath) {
      names = Array
        .range(1, kfolds + 1)
        .map(value => Transformations.getOutPutForGCS(table, value.toString))
    }

    logger.info("datasets names with partitions")
    logger.info(names.mkString("\n"))

    val arrayDFNames = {
      if (!isComputed && isFormatted) {

        val weigths = Array.fill(kfolds)(1.0 / kfolds)

        val splitDF = dfTable.randomSplit(weigths, seed)

        for (numFold <- 0 to kfolds - 1) {
          val name = names(numFold)
          if (isGCPPath) {

            val namePath =
              splitDF(numFold).write
                .mode(SaveMode.Overwrite)
                .format("parquet")
                .save(name)

          } else {
            splitDF(numFold).write.mode(SaveMode.Overwrite).saveAsTable(name)
          }
        }

        names

      } else if (isComputed && isFormatted) {

        names

      } else {

        throw new IllegalArgumentException(
          "The Table selected dont have the correct format"
        )

      }
    }

    arrayDFNames
  }

  def traingTestSplit(
      spark: SparkSession,
      foldTest: String,
      foldsTraining: Array[String],
      out: String,
      numPartitions: Int,
      isGCPPath: Boolean = false
  ): Array[DataFrame] = {

    logger.info("........test fold: " + foldTest + "...............")
    logger.info(
      "........train folds: " + foldsTraining.mkString(
        ","
      ) + "..............."
    )
    // preload the first combiantion
    val s = foldsTraining.size
    var dataTrain =
      if (isGCPPath) spark.read.parquet(foldsTraining(0))
      else spark.table(foldsTraining(0))

    val dataTest: DataFrame =
      if (isGCPPath) spark.read.parquet(foldTest)
      else spark.table(foldTest)

    for (fold <- foldsTraining.slice(1, s)) {

      dataTrain =
        if (isGCPPath) dataTrain.union(spark.read.parquet(fold))
        else dataTrain.union(spark.table(fold))
    }
    if (isGCPPath) {
      dataTrain.write
        .mode(SaveMode.Overwrite)
        .format("parquet")
        .save(out + "train/")

      dataTest.write
        .mode(SaveMode.Overwrite)
        .format("parquet")
        .save(out + "test/")

      val train = spark.read.parquet(out + "train/").repartition(numPartitions)
      val test = spark.read.parquet(out + "test/").repartition(numPartitions)
      Array(train, test)

    } else {
      dataTest.write.mode(SaveMode.Overwrite).saveAsTable(out + "_test")
      dataTrain.write.mode(SaveMode.Overwrite).saveAsTable(out + "_train")
      val train = spark.table(out + "_train").repartition(numPartitions)
      val test = spark.table(out + "_test").repartition(numPartitions)
      Array(train, test)
    }

  }

  def getPathNames(
      isGCPPath: Boolean,
      output: String
  ): (String, String, String) = {
    var outPutFile = ""
    var pathToUpload = ""
    if (isGCPPath) {
      val reportName = output.split("/").last
      outPutFile = "reports/" + reportName + ".csv"
      pathToUpload = output + "reports/" + reportName + "/"
    } else {
      outPutFile = "reports/" + output + ".csv"

    }
    val tableOutput =
      if (isGCPPath) output + "outputs/"
      else "table_" + output

    (outPutFile, pathToUpload, tableOutput)

  }

  def UploadToGcs(
      isGCPPath: Boolean,
      outPutFile: String,
      pathToUpload: String,
      numExperiment: Int
  ): Unit = {

    if (isGCPPath) {
      val currentDirectory: String =
        new java.io.File(".").getCanonicalPath

      logger.info(
        "uploading the file with temporal results:" + outPutFile
      )

      val storage = StorageOptions.getDefaultInstance.getService
      val (bucket, basePath) = getBucketFromPath(pathToUpload)
      val blobId = BlobId.of(bucket, basePath)
      val blobInfo = BlobInfo.newBuilder(blobId).build
      logger.info("the path will be: " + pathToUpload)

      storage.create(
        blobInfo,
        Files.readAllBytes(
          Paths.get("/" + currentDirectory + "/" + outPutFile)
        )
      )

      logger.info("finish UPLOAD: " + numExperiment.toString)
    }

  }
}
