resolvers += "jitpack" at "https://jitpack.io"

name := "lsh-analysis"

version := "0.4.6"

scalaVersion := "2.12.17"

sparkVersion := "2.4.0"

sparkComponents ++= Seq("sql", "mllib", "hive")

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x                             => MergeStrategy.first
}

libraryDependencies += "com.github.mrpowers" % "spark-fast-tests" % "v2.3.0_0.12.0" % "test"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.6.0"

libraryDependencies += "com.google.cloud" % "google-cloud-storage" % "2.12.0"

fork in run := true

javaOptions in run ++= Seq(
  "-Dlog4j.debug=false",
  "-Dlog4j.configuration=log4j.properties"
)
outputStrategy := Some(StdoutOutput)

// test suite settings
fork in Test := true
javaOptions ++= Seq(
  "-Xms512M",
  "-Xmx2048M",
  "-XX:MaxPermSize=2048M",
  "-XX:+CMSClassUnloadingEnabled"
)

javaOptions in Test ++= Seq("-Dlog4j.debug=false" + "-Dlog4j.info=false")
outputStrategy := Some(StdoutOutput)

// JAR file settings
assemblyOption in assembly := (assemblyOption in assembly).value
  .copy(includeScala = false)

// Add the JAR file naming conventions described here: https://github.com/MrPowers/spark-style-guide#jar-files
// You can add the JAR file naming conventions by running the shell script
artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "_" + sv.binary + "-" + sparkVersion.value + "_" + module.revision + "." + artifact.extension
}
// To ignore test in assembly
test in assembly := {}

assemblyJarName in assembly := s"${name.value}_${scalaBinaryVersion.value}-${sparkVersion.value}_${version.value}.jar"
