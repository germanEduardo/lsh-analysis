# lsh-analysis

This project wraps experiment to perform new techniques of Instance Selection based on Locality Sensitive Hashing.


## Documentation

### Perform experiments on GCLOUD

1. Enable the subnet
```bash
gcloud compute networks subnets update default \
--region=us-central1 \
--enable-private-ip-google-access 
```
2. From csv to parquet

```bash
gcloud dataproc batches submit spark \
--region=us-central1 \
--jars=gs://dataproc-udea/clases/lsh-analysis_2.12-2.4.0_0.4.6.jar \
--class=com.org.udea.runners.PreProcessDataGCP \
-- -I gs://dataproc-udea/data/pageblocks/ \
-p 12 \
-F "height,lenght,area,eccen,p_black,p_and,mean_tr,blackpix,blackand,wb_trans" \
-o gs://dataproc-udea/output/dataset/pageblocks/ \
-l class \
-i class \
-L 1=1,0=0

gcloud dataproc batches submit spark \
--region=us-central1 \
--async \
--jars=gs://dataproc-udea/clases/lsh-analysis_2.12-2.4.0_0.4.6.jar \
--class=com.org.udea.runners.PreProcessDataGCP \
-- -I gs://dataproc-udea/data/fraudk/ \
-p 12 \
-F "V1,V2,V3,V4,V5,V6,V7,V8,V9,V10,V11,V12,V13,V14,V15,V16,V17,V18,V19,V20,V21,V22,V23,V24,V25,V26,V27,V28,Amount" \
-o gs://dataproc-udea/output/dataset/fraudk/ \
-l class \
-i class \
-L 1=1,0=0



```



nohup spark2-submit --driver-memory 12G \
--num-executors 5 --executor-cores 2 --executor-memory 18G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=4800 \
--class "com.org.udea.runners.PreProcessData"


### Perfom Experiments

#### with Page Blocks DataSet

nohup spark2-submit --driver-memory 9G \
--num-executors 5 --executor-cores 2 --executor-memory 16G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=3800 \
--class "com.org.udea.runners.RunIsLshRf" lsh-analysis_2.11-2.3.0_0.1.0.jar \
-I pageblocksDB -p 50 -J jsons/PB.json \
-L 1,0 -o pageblocksEntropy -C 0 -k 5 -S 1 \
-t 10,25,50 -d 10,20,30 \
-A 2,4,6,8,10 -O 1 -B 0,1 \
-l hyperplanes,projection -z entropy -s 0.1 \
-n 4 -m 1000 -D 6 > pageblocksEntropy 2>&1&


gcloud dataproc batches submit spark \
--region=us-central1 \
--jars=gs://dataproc-udea/clases/lsh-analysis_2.12-2.4.0_0.4.6.jar \
--class=com.org.udea.runners.RunRandom \
--files=gs://dataproc-udea/clases/noCatIndex.json \
-- -I gs://dataproc-udea/output/dataset/pageblocks/ \
-p 12 \
-J noCatIndex.json \
-o gs://dataproc-udea/output/results/pageblocks/randomSubSampling/ \
-C 1 \
-k 5 \
-t 10,25,50 \
-d 10,20,30  \
-z undersampling


nohup spark2-submit --driver-memory 9G \
--num-executors 3 --executor-cores 2 --executor-memory 16G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=3800 \
--class "com.org.udea.runners.RunIsLshRf" lsh-analysis_2.11-2.3.0_0.2.0.jar \
-I pageblocksDB -p 50 -J jsons/PB.json \
-L 1,0 -o pageblocksEntropy -C 0 -k 5 -S 1 \
-t 10,25,50 -d 10,20,30 \
-A 2,4,6,8,10 -O 1 -B 0,1 \
-l hyperplanes-projection -z entropy -s 0.1 \
-n 4 -m 1000 -D 6 > pageblocksEntropy-comb 2>&1&



#### with Fraudk DataSet
nohup spark2-submit --driver-memory 9G \
--num-executors 2 --executor-cores 2 --executor-memory 10G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=3800 \
--class "com.org.udea.runners.RunIsLshRf" lsh-analysis_2.11-2.3.0_0.1.0.jar \
-I fraudk -p 100 -J jsons/PB.json \
-L 1,0 -o fraudkEntropy -C 1 -k 5 -S 4 \
-t 10,25,50 -d 10,20,30 \
-A 2,4,6,8,10 -O 1 -B 0,1 \
-l hyperplanes,projection -z entropy -s 0.1 \
-n 4 -m 1000 -D 6 > fraudkEntropy 2>&1&


nohup spark2-submit --driver-memory 12G --num-executors 2 --executor-cores 2 \
--executor-memory 18G --conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=4800 \
--class "com.org.udea.runners.RunIsLshRf" \
lsh-analysis_2.11-2.3.0_0.4.1.jar -I fraudk_id_db \
-p 100 -J jsons/PB.json -L 1,0 -o fraudDrop3 -C 0 -k 5 -S 1 -t 10,25,50 \
-d 10,20,30 -A 2,4,6,8,10 -O 1 -B 0,1 -l hyperplanes,projection,hyperplanes-projection \
-z drop3 -s 0.1 -n 4 -m 1000 -D 6 -N 0 > fraudDrop3 2>&1&



nohup spark2-submit --driver-memory 9G \
--num-executors 2 --executor-cores 2 --executor-memory 10G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=3800 \
--class "com.org.udea.runners.RunIsLshRf" lsh-analysis_2.11-2.3.0_0.4.1.jar \
-I fraudk_id_db -p 100 -J jsons/PB.json \
-L 1,0 -o fraudkDrop3 -C 0 -k 5 -S 1 \
-t 10,25,50 -d 10,20,30 \
-A 2,4,6,8,10 -O 1 -B 0,1 \
-l hyperplanes,projection,hyperplanes-projection -z entropy -s 0.1 \
-n 4 -m 1000 -D 6 > fraudkEntropy-comb 2>&1&


gcloud dataproc batches submit spark \
--async \
--region=us-central1 \
--jars=gs://dataproc-udea/clases/lsh-analysis_2.12-2.4.0_0.4.6.jar \
--class=com.org.udea.runners.RunIsLshRf \
--files=gs://dataproc-udea/clases/noCatIndex.json \
-- -I gs://dataproc-udea/output/dataset/fraudk/ \
-p 12 \
-J noCatIndex.json \
-o gs://dataproc-udea/output/results/fraudk/IS_ENTROPY/ \
-C 1 \
-k 5 \
-t 10,25,50 \
-d 10,20,30  \
-A 2,4,6,8,10 \
-O 1 \
-B 0 \
-l hyperplanes,projection,hyperplanes-projection \
-z entropy \
-s 0.1 -n 4 -m 1000 -D 6





gcloud dataproc batches submit spark \
--async \
--region=us-central1 \
--jars=gs://dataproc-udea/clases/lsh-analysis_2.12-2.4.0_0.4.6.jar \
--class=com.org.udea.runners.RunRandom \
--files=gs://dataproc-udea/clases/noCatIndex.json \
-- -I gs://dataproc-udea/output/dataset/fraudk/ \
-p 12 \
-J noCatIndex.json \
-o gs://dataproc-udea/output/results/fraudk/randomSubSampling/ \
-C 0 \
-k 5 \
-t 10,25,50 \
-d 10,20,30  \
-z undersampling



#### with susy DataSet
nohup spark2-submit --driver-memory 12G \
--num-executors 3 --executor-cores 2 --executor-memory 18G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=4800 \
--class "com.org.udea.runners.RunIsLshRf" lsh-analysis_2.11-2.3.0_0.2.1.jar \
-I susyDB -p 200 -J jsons/PB.json \
-L 1,0 -o susyEntropy -C 1 -k 5 -S 1 \
-t 10,25,50 -d 10,20,30 \
-A 2,4,6,8,10 -O 1 -B 1 \
-l hyperplanes,projection,hyperplanes-projection -z entropy -s 0.1 \
-n 4 -m 1000 -D 6 -N 0 > susyEntropy 2>&1&


#### with P2P dataset
first have to have preprocess the database:

nohup spark2-submit --driver-memory 12G \
--num-executors 4 --executor-cores 2 --executor-memory 18G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=4800 \
--class "com.org.udea.runners.PreProcessData" lsh-analysis_2.11-2.3.0_0.4.0.jar \
-I variables_finales_tarjeta -p 20 -F baseNoMoneda.txt \
-f "(monto<100000000) AND (label!=0) AND (resp_code=1 OR resp_code=2) AND (unix_timestamp(fecha) < 1433221200)" \
-L -1=0,1=1 -o p2p_db -l label -i idn> p2p_db 2>&1&


nohup spark2-submit --driver-memory 12G \
--num-executors 5 --executor-cores 2 --executor-memory 18G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=4800 \
--class "com.org.udea.runners.RunIsLshRf" lsh-analysis_2.11-2.3.0_0.4.0.jar \
-I p2p_db -p 150 -J jsons/categoryMapsNoMon.json \
-L 1,0 -o p2p_entropy -C 1 -k 5 -S 1 \
-t 10,25,50 -d 10,20,30 \
-A 2,4,6,8,10 -O 1 -B 1 \
-l hyperplanes,projection,hyperplanes-projection -z entropy -s 0.1 \
-n 4 -m 1000 -D 6 -N 0 > p2p_entropy 2>&1&

## Run pre-process
nohup spark2-submit --driver-memory 12G \
--num-executors 5 --executor-cores 2 --executor-memory 18G \
--conf spark.dynamicAllocation.enabled=false \
--conf spark.network.timeout=4800 \
--class "com.org.udea.runners.PreProcessData"




## Special set up for compile in windows

1. Download the binary file of [wintuils.exe](https://github.com/steveloughran/winutils/blob/master/hadoop-2.8.1/winutils.exe?raw=true)
2. Create hadoop folder in Your System, ex `C`:
3. Create bin folder in hadoop directory, ex : `C:\hadoop\bin`
3. Paste `winutils.exe` in `bin`, ex: `C:\hadoop\bin\winuitls.exe`
4. In User Variables in System Properties -> Advance System Settings
Create New Variable Name: ``HADOOP_HOME`` Path: ``C:\hadoop\``
5. the winutils need the 
[Microsoft Visual C++ 2010 Redistributable Package (x64)](https://www.microsoft.com/en-us/download/confirmation.aspx?id=14632)
be installed. (the link it is for 2020-08-28).
5. restart your IDE.

## How to contribute

*How others can contribute to the project*